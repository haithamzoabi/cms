(function ($) {
    'use strict';

    $.fn.bootstrapTable.locales['HE'] = {
        formatLoadingMessage: function () {
            return 'טוען...';
        },
        formatRecordsPerPage: function (pageNumber) {
            return pageNumber + ' שורות לדף';
        },
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'מציג ' + pageFrom + ' עד ' + pageTo + ' מ ' + totalRows + ' שורות';
        },
        formatSearch: function () {
            return 'חיפוש';
        },
        formatNoMatches: function () {
            return 'אין תוצאות';
        }
    };

    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['HE']);

})(jQuery);