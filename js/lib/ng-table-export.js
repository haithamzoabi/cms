var ngTableExport = angular.module('ngTableExport', []);
ngTableExport.config(['$compileProvider', function ($compileProvider) {
        // allow data links
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|data):/);
    }]);
ngTableExport.directive('exportCsv', ['$parse', '$timeout', function ($parse, $timeout) {
        var delimiter = ',\t';
        var header = 'data:application/octet-stream;charset=UTF-8,%EF%BB%BF';
        return {
            restrict: 'A',
            scope: false,
            /**
             * scope is table scope, element is <table>
             */
            link: function (scope, element, attrs) {
                var data = '';
                // allow pass in of delimiter via directive attrs
                if (attrs.delimiter) {
                    delimiter = attrs.delimiter;
                }
                function stringify(str) {
                    // trim spaces and replace quotes with double quotes
                    return str.replace(/^\s\s*/, '').replace(/\s*\s$/, '').replace(/"/g, '""');
                }
                /**
                 * Parse the table and build up data uri
                 */
                function parseTable() {
                    data = '';
                    var rows = element.find('tr');
                    angular.forEach(rows, function (row, i) {
                        var tr = angular.element(row),
                                tds = tr.find('th'),
                                rowData = '';
                        if (tr.hasClass('ng-table-filters')) {
                            return;
                        }
                        if (tds.length === 0) {
                            tds = tr.find('td');
                        }
						angular.forEach(tds, function (td) {
							// respect colspan in row data
							if (td.innerText!==""){
								rowData += stringify(td.innerText) + Array.apply(null, Array(td.colSpan)).map(function () {
									return delimiter;
								}).join('');
							}
						});
						rowData = rowData.slice(0, rowData.length - 1); //remove last semicolon						
                        data += rowData + '\n';
                    });
                    // add delimiter hint for excel so it opens without having to import
                    //data = 'sep=' + delimiter + '\n' + data;
                }

                /**
                 * Dynamically generate a link and click it; works in chrome + firefox; unfortunately, safari
                 * does not support the `download` attribute, so it ends up opening the file in a new tab https://bugs.webkit.org/show_bug.cgi?id=102914
                 */
                function download(dataUri, filename) {
                    // tested in chrome / firefox / safari
                    var link = document.createElement('a');
                    // chrome + firefox
                    link.style.display = 'none';
                    link.href = dataUri;
                    link.download = filename;
                    link.target = '_blank';
                    // needs to get wrapped to play nicely with angular $digest
                    // else may cause '$digest already in progress' errors with other angular controls (e.g. angular-ui dropdown)
                    $timeout(function () {
                        // must append to body for firefox; chrome & safari don't mind
                        document.body.appendChild(link);
                        link.click();
                        // destroy
                        document.body.removeChild(link);
                    }, 0, false);
                }

                var csv = {
                    /**
                     *  Generate data URI from table data
                     */
                    generate: function (event, filename) {
                        if (!filename){
                            filename = new Date().format("mm-dd-yyyy-HHMM")+".csv";
                        }
                        parseTable();
                        download(header + encodeURIComponent(data), filename);
                    }
                };

                // attach csv to table scope
                $parse(attrs.exportCsv).assign(scope.$parent, csv);
            }
        };
    }]);