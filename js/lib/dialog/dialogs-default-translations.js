angular.module('dialogs.default-translations',['dialogs.services','ngSanitize']).config(['$translateProvider',
	function ($translateProvider) {

		$translateProvider.translations('en-US', {
			DIALOGS_ERROR: "Error",
			DIALOGS_ERROR_MSG: "An unknown error has occurred.",
			DIALOGS_CLOSE: "Close",
			DIALOGS_PLEASE_WAIT: "Please Wait",
			DIALOGS_PLEASE_WAIT_ELIPS: "Please Wait...",
			DIALOGS_PLEASE_WAIT_MSG: "Waiting on operation to complete.",
			DIALOGS_PERCENT_COMPLETE: "% Complete",
			DIALOGS_NOTIFICATION: "Notification",
			DIALOGS_NOTIFICATION_MSG: "Unknown application notification.",
			DIALOGS_CONFIRMATION: "Confirmation",
			DIALOGS_CONFIRMATION_MSG: "Confirmation required.",
			DIALOGS_OK: "OK",
			DIALOGS_YES: "Yes",
			DIALOGS_NO: "No"
		});

		$translateProvider.translations('he', {
			DIALOGS_ERROR: "שגיאה",
			DIALOGS_ERROR_MSG: "אירעה שגיאה לא ידועה.",
			DIALOGS_CLOSE: "סגור",
			DIALOGS_PLEASE_WAIT: "אנא המתן",
			DIALOGS_PLEASE_WAIT_ELIPS: "אנא המתן...",
			DIALOGS_PLEASE_WAIT_MSG: "ממתין לסיום פעולה.",
			DIALOGS_PERCENT_COMPLETE: "% הסתיים",
			DIALOGS_NOTIFICATION: "הודעה",
			DIALOGS_NOTIFICATION_MSG: "הודעת מערכת לא ידועה",
			DIALOGS_CONFIRMATION: "אישור",
			DIALOGS_CONFIRMATION_MSG: "אישור נדרש.",
			DIALOGS_OK: "אישור",
			DIALOGS_YES: "כן",
			DIALOGS_NO: "לא"
		});

		//$translateProvider.preferredLanguage('he');
	}
]); // end config
