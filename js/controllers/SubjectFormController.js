appControllers.controller('SubjectFormController',[
    '$scope', '$filter', '$http', '$routeParams', 'loadMask',
    function($scope, $filter, $http, $routeParams, loadMask){
        $scope.postId = $routeParams.postId;
        $scope.errorMessage = '';
        $scope.subjectData = {
            visible: false
        }; 
        $scope.typeOptions = [
            {
                text: $filter('xlat')('textKeys.simpleContent'),
                value: 0
            },
            {
                text: $filter('xlat')('textKeys.extended'),
                value: 1
            },
            {
                text: $filter('xlat')('textKeys.category'),
                value: 2
            }
        ];
        $scope.subjectData.type = $scope.typeOptions[0];

        var fetchSubjectData = function(id) {
            var loadingText = $filter('xlat')('textKeys.loading');
            loadMask.showLoading(loadingText,'SubjectFormController');
            var postUrl = 'server/subjects.php?id='+id;
            $.ajax({
                type: "GET",
                url: postUrl,
                success: function (response) {
                    if (response.success){
                        $scope.subjectData = response.data;
                        $scope.subjectData.type = $scope.typeOptions[Number($scope.subjectData.type)];
                        $scope.subjectData.visible =  $scope.subjectData.visible==="1";
                        $scope.onTypeChnage();
                        $scope.$apply();
                    } else { 
                        $scope.errorMessage = $filter('xlat')( response.errorMessage );
                    }
                    loadMask.hideLoading('SubjectFormController');
                },
                error: function(){
                    $scope.errorMessage = $filter('xlat')("textKeys.COULD_NOT_FETCH_DATA");
                    $scope.$apply();
                    loadMask.hideLoading('SubjectFormController');
                },
                dataType: 'json'
            })

        };
        if (Number($scope.postId)){
            fetchSubjectData($scope.postId);
        }

        $scope.onTypeChnage = function(){
            var typeValue = $scope.subjectData.type.value;
            $scope.contentVisible = typeValue === 1;
            $scope.galleryVisbile = typeValue !== 0;
        }
        $scope.onTypeChnage();

        var addNewSubject = function(){

            $scope.subjectData.sType = $scope.subjectData.type.value;
            $scope.subjectData.menuId = $scope.$parent.menuItemId;
            var postUrl = 'server/subjects.php?query=new';
            $.ajax({
                type: "POST",
                url: postUrl,
                data: $.param($scope.subjectData),
                success: function (response) {
                    if (response.success){
                        $scope.$emit("closeCurrentTab");
                    } else { 
                        $scope.errorMessage = $filter('xlat')( response.errorMessage );    
                    }
                },
                error: function(){
                    $scope.errorMessage = $filter('xlat')("textKeys.ERROR_ON_SAVE");
                },
                dataType: 'json'
            });
            
        };
        
        var updateSubject = function(){
            $scope.subjectData.sType = $scope.subjectData.type.value;
            $scope.subjectData.menuId = $scope.$parent.menuItemId;
            var postUrl = 'server/subjects.php?query=update';
            $.ajax({
                type: "POST",
                url: postUrl,
                data: $.param($scope.subjectData),
                success: function (response) {
                    if (response.success){
                        $scope.$emit("closeCurrentTab");
                    } else { 
                        $scope.errorMessage = $filter('xlat')( response.errorMessage )||response.errorMessage;    
                    }
                },
                error: function(){
                    $scope.errorMessage = $filter('xlat')("textKeys.ERROR_ON_SAVE");
                },
                dataType: 'json'
            });
        };


        $scope.saveSubject = function(){
            if ( Number($scope.postId) ){
                updateSubject();
            } else if($scope.postId === 'new') {
                addNewSubject();                
            }            
        };
    }
]);