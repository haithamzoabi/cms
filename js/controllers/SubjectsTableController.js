appControllers.controller('SubjectsTableController',[
    '$scope', '$modal', '$filter', '$routeParams', '$compile', '$http', 'dialogs',
    function (scope, $modal, filter, $routeParams, $compile, $http, dialogs) {

        scope.selectedItems = [];
        scope.menuItemId = $routeParams.menuItemId;
        var tableDataUrl = 'server/subjects.php?query=all&menuId='+scope.menuItemId;
        var subjectsTable = $("#subjects-table");
        var updateSelectedItems = function () {
            scope.selectedItems = subjectsTable.bootstrapTable("getSelections");
            scope.$digest();
        };
        var reloadTableData = function(){
            subjectsTable.bootstrapTable('refresh', {
                url: tableDataUrl
            });
        };
        var CONTENT_TYPES_MAP = [
            { value : 0 , text: "simpleContent"},
            { value : 1 , text: "extended"},
            { value : 2 , text: "category"},
        ]

        scope.subjectsTableOtions = {
            height: 550,
            onLoadSuccess: function () {
                $compile(subjectsTable.contents())(scope);
                scope.selectedItems = [];
            },
            onCheck: updateSelectedItems,
            onCheckAll: updateSelectedItems,
            onUncheck: updateSelectedItems,
            onUncheckAll: updateSelectedItems,
            onClickRow: function (row, element) {
                var index = $(element).data('index');
                subjectsTable = $("#subjects-table");
                subjectsTable.bootstrapTable('uncheckAll');
                subjectsTable.bootstrapTable('check', index);
                updateSelectedItems();
            },
            url: tableDataUrl,
            striped: true,
            pagination: false,
            pageSize: 50,
            pageList: [10, 25, 50, 100, 200],
            search: true,
            iconSize: 'sm', 
            minimumCountColumns: 2,
            clickToSelect: false,
            selectItemName: 'radioName',
            showColumns: true,
            showRefresh: true,
            showToggle: false,
            columns: [
                {
                    field: 'state',
                    checkbox: 'true',
                },
                {
                    title: '#',
                    field: 'id',
                    width: '25',
                    formatter: function (value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: filter('xlat')('textKeys.title'),
                    field: 'title',
                    sortable: true,
                    formatter: function(value, row, index){
                        var href = '#/menuItem/'+scope.menuItemId+'/subjects/'+row.id;
                        value = '<a href="'+href+'" >'+value+'</a>';
                        return "<span class='rowIdSpan'>"+row.id+"#</span> "+value;
                    }
                },
                {
                    title: filter('xlat')('textKeys.contentType'),
                    field: 'type',
                    sortable: true,
                    formatter: function(value, row, index){
                        var displayText = filter('xlat')('textKeys.'+CONTENT_TYPES_MAP[value].text);;
                        if (value === '2'){
                            var href = '#/menuItem/'+scope.menuItemId+'/posts?category='+row.id;
                            displayText = '<a href="'+href+'" >'+displayText+'</a>';
                        }
                        return displayText;
                    }
                },
                {
                    title: filter('xlat')('textKeys.datecreated'),
                    field: 'created',
                    sortable: true
                },
                {
                    title: filter('xlat')('textKeys.dateupdated'),
                    field: 'updated',
                    sortable: true,
                    formatter: function(value, row, index){
                        if (!value){
                            return "<span class='rowIdSpan'>"+filter('xlat')('textKeys.notUpdated')+"</span>";
                        }
                        return value;
                    }
                },
                {
                    title: filter('xlat')('textKeys.visible'),
                    field: 'visible',
                    sortable: true,
                    formatter: function(value, row, index){
                        var element = value==1?'<span class="fa fa-check" ></span>':'<span class="fa fa-times" ></span>';
                        return element;
                    }
                },
                {
                    title: filter('xlat')('textKeys.createdBy'),
                    field: 'author',
                    sortable: true                
                }
            ]
        };


        scope.addSubject = function () {
            var params = {
                id: "new",
                type: "subjects"
            }
            scope.$emit("openNewTab", params );    
        };
        scope.editSubject = function () {
            var selectedItem = scope.selectedItems[0];
            var params = {
                id: selectedItem.id,
                type: "subjects"
            }
            scope.$emit("openNewTab", params );
        };

        var deleteItemAfterConfirm = function () {
            var deleteFailureText = filter('xlat')('textKeys.deleteFailureText');
            var selectedRows = scope.selectedItems;
            var idsToDelete = [];
            for (var i in selectedRows) {
                var row = selectedRows[i];
                idsToDelete.push(row.id);
            }

            var request = {
                url: 'server/subjects.php?query=delete',
                method: 'DELETE',
                data: {idsToDelete: idsToDelete}
            };
            $http(request).then(function (response) {
                var data = response.data;             
                if (data.success) {
                    reloadTableData();
                } else {
                    dialogs.error(undefined,deleteFailureText);
                }
            });
        };

        scope.deleteSubject = function () {
            var confirmTitle = filter('xlat')('textKeys.warning');
            var deleteEventConfirm = filter('xlat')('textKeys.deleteEventConfirm');
            var confirm = dialogs.confirm(confirmTitle, deleteEventConfirm);
            confirm.result.then(deleteItemAfterConfirm);
        };


    }
]);