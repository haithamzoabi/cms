appControllers.controller('MainController', [
    '$scope', '$location', '$rootScope', '$http', '$filter','$modal', 'usersFactory', 'mainHelper',
    function ($scope, $location, $rootScope, $http, $filter,$modal, usersFactory, mainHelper) {
        
        $scope.Page = mainHelper;
        var changeLocation = function (value) {
            var currentPath = $location.$$path;
            if (value===false){
                if (currentPath != "/login") {
                    $location.path("/login");
                }
            }else if(value===true) {
                if ((currentPath === "" || currentPath === "/" || currentPath === "/login") && $rootScope.menuItems) {
                    $location.path($rootScope.menuItems[0].link);
                }
            }
        };

        $scope.$watch('authenticated', changeLocation );

        $scope.$on('$routeChangeStart', function (next, current) {
            changeLocation( $rootScope.authenticated );
        });


        $scope.authenticate = function (callback) {

            $scope.currentPage = $location.$$path;
            usersFactory.getCurrentUserData().then(function(response){
                if (response.success){
                    var data = response.data;
                    $scope.username = data.name;
                    $rootScope.authenticated = !!data.loginId;
                    callback && callback(data);
                } else {
                    $rootScope.authenticated = false;
                    callback && callback();
                }
            });

        };

        if (!$rootScope.authenticated) {
            $scope.authenticate();
        }

        $scope.logout = function () {
            $http.post('server/Logout.php', {}).then(function () {
                $scope.authenticate();
            });
        };



    }
]);