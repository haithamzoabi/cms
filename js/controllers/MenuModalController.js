appControllers.controller('MenuModalController', ['$scope', '$modalInstance','modalParams', '$filter' ,
    function($scope, modalInstance,modalParams, filter){
        $scope.menuData = modalParams.data;

        $scope.errorMessage = filter('xlat')('textKeys.formNotValid');
        $scope.cancel = function () {
			modalInstance.dismiss();
		};
        $scope.save = function(){            
            var postUrl = "server/saveMenuItem.php";
            $.ajax({
				type: "POST",
				url: postUrl,
				data: $.param($scope.menuData),
				success: function (response) {
					if (response.success) {						
						modalInstance.close();
					} else {
						$scope.errorMessage = response.errorMessage;
						$scope.errorVisible = true;
						$scope.$digest();
					}
				},
				dataType: 'json'
			});            
        }
                
        
    }
]);