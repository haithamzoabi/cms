appControllers.controller('PostsTableController',[
    '$scope', '$modal', '$filter', '$routeParams', '$compile', '$http', 'dialogs',
    function (scope, $modal, filter, $routeParams, $compile, $http, dialogs) {
        
        scope.selectedItems = [];
        scope.menuItemId = $routeParams.menuItemId;
        scope.categoryId = $routeParams.category? $routeParams.category : '';
        scope.addBtnTooltip = scope.categoryId==='' ? filter('xlat')('textKeys.addBtnTooltip_cannotAdd') : "";
        var menuIdParam = scope.menuItemId ? "&menuId="+scope.menuItemId : "";
        var categoryIdParam = scope.categoryId ? "&categoryId="+scope.categoryId : "";        
        var tableDataUrl = String.format("server/posts.php?query=all{0}{1}" , menuIdParam , categoryIdParam );
        
        var postsTable = $("#posts-table");
        var updateSelectedItems = function () {
            scope.selectedItems = postsTable.bootstrapTable("getSelections");
            scope.$digest();
        };
        var reloadTableData = function(){
            postsTable.bootstrapTable('refresh', {
                url: tableDataUrl
            });
        };
        scope.postsTableOtions = {
            height: 550,
            onLoadSuccess: function () {
                $compile(postsTable.contents())(scope);
                scope.selectedItems = [];
            },
            onCheck: updateSelectedItems,
            onCheckAll: updateSelectedItems,
            onUncheck: updateSelectedItems,
            onUncheckAll: updateSelectedItems,
            onClickRow: function (row, element) {
                var index = $(element).data('index');
                postsTable = $("#posts-table");
                postsTable.bootstrapTable('uncheckAll');
                postsTable.bootstrapTable('check', index);
                updateSelectedItems();
            },
            url: tableDataUrl,
            striped: true,
            pagination: false,
            pageSize: 50,
            pageList: [10, 25, 50, 100, 200],
            search: true,
            iconSize: 'sm', 
            minimumCountColumns: 2,
            clickToSelect: false,
            selectItemName: 'radioName',
            showColumns: true,
            showRefresh: true,
            showToggle: false,
            columns: [
                {
                    field: 'state',
                    checkbox: 'true',
                },
                {
                    title: '#',
                    field: 'id',
                    width: '25',
                    formatter: function (value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: filter('xlat')('textKeys.title'),
                    field: 'title',
                    sortable: true,
                    formatter: function(value, row, index){
                        var href = '#/menuItem/'+scope.menuItemId+'/posts/'+row.id;
                        value = '<a href="'+href+'" >'+value+'</a>';
                        return "<span class='rowIdSpan'>"+row.id+"#</span> "+value;
                    }
                },
                {
                    title: filter('xlat')('textKeys.datecreated'),
                    field: 'created',
                    sortable: true
                },
                {
                    title: filter('xlat')('textKeys.dateupdated'),
                    field: 'updated',
                    sortable: true,
                    formatter: function(value, row, index){
                        if (!value){
                            return "<span class='rowIdSpan'>"+filter('xlat')('textKeys.notUpdated')+"</span>";
                        }
                        return value;
                    }
                },
                {
                    title: filter('xlat')('textKeys.visible'),
                    field: 'visible',
                    sortable: true,
                    formatter: function(value, row, index){
                        var element = value==1?'<span class="fa fa-check" ></span>':'<span class="fa fa-times" ></span>';
                        return element;
                    }
                },
                {
                    title: filter('xlat')('textKeys.createdBy'),
                    field: 'author',
                    sortable: true                
                }
            ]
        };
        
        scope.addPost = function () {
            if (scope.categoryId === ''){
                return;
            }
            var params = {
                id: "new",
                type: "posts"
            }
            scope.$emit("openNewTab", params );    
        };
        scope.editPost = function () {
            var selectedItem = scope.selectedItems[0];
            var params = {
                id: selectedItem.id,
                type: "posts"
            }
            scope.$emit("openNewTab", params );
        };

        var deleteItemAfterConfirm = function () {
            var deleteFailureText = filter('xlat')('textKeys.deleteFailureText');
            var selectedRows = scope.selectedItems;
            var idsToDelete = [];
            for (var i in selectedRows) {
                var row = selectedRows[i];
                idsToDelete.push(row.id);
            }

            var request = {
                url: 'server/posts.php?query=delete',
                method: 'DELETE',
                data: {idsToDelete: idsToDelete}
            };
            $http(request).then(function (response) {
                var data = response.data;             
                if (data.success) {
                    reloadTableData();
                } else {
                    dialogs.error(undefined,deleteFailureText);
                }
            });
        };

        scope.deletePost = function () {
            var confirmTitle = filter('xlat')('textKeys.warning');
            var deleteEventConfirm = filter('xlat')('textKeys.deleteEventConfirm');
            var confirm = dialogs.confirm(confirmTitle, deleteEventConfirm);
            confirm.result.then(deleteItemAfterConfirm);
        };

    }
]);
