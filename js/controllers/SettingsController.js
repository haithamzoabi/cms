appControllers.controller("SettingsController", [
    "$scope","$routeParams","$location",'usersFactory', '$filter','WebSettingsFactory',
    function (scope , routeParams, location,usersFactory, filter,WebSettingsFactory) {
        var self = this;
        var param = routeParams.profilePage;
        var messageTypes ={
            SUCCESS : "alert-success",
            ERROR: "alert-danger"
        };

        self.settinsPages = {
            "website-settings": {
                url: "pages/web-settings.php",
                data: function(){
                    loadWebsiteData();
                }
            },
            "profile": {
                url: "pages/profile.php",
                data: function(){
                    loadUserData();
                }
            },
            "language":{
                url: "pages/language-vars.php",
                data: function(){}
            }
        }
        scope.param = param;
        scope.pageToInclude = self.settinsPages[param].url;
        self.settinsPages[param].data();


        function loadWebsiteData(){
            scope.webData= {};
            var data = WebSettingsFactory.getSettings().then(function(response){
                if(response.data){
                    scope.webData = response.data;
                }
            });
        }

        function setFormMessage(show,type,text,form){
            scope.messageSettings={
                form: form ? form : "",
                show : show ? show : false,
                type:type ? type : messageTypes.SUCCESS,
                text: text ? text : ""
            }
        }
        scope.update =function(userData){
            usersFactory.updateUser(scope.userData).done(function(response){
                var obj =JSON.parse(response) ;
                if(obj.success){
                    scope.userData = obj.data;
                    setFormMessage(true,messageTypes.SUCCESS, filter('xlat')('textKeys.alert.changes.saved'),"profileForm");
                    scope.$apply();
                }
            });
        };

        scope.updateWebSettings =function(){
            WebSettingsFactory.updateSettings(scope.webData).done(function(response){
                var obj =JSON.parse(response) ;
                if(obj.success){
                    scope.webData = obj.data;
                    setFormMessage(true,messageTypes.SUCCESS, filter('xlat')('textKeys.alert.changes.saved'),"webSettingsForm");
                    scope.$apply();
                }
            });
        };

        function loadUserData(){
            scope.userData = {};
            var data = usersFactory.getCurrentUserData().then(function(response){
                scope.userData = response.data;
            });
        };

        setFormMessage();
        scope.userPssword={};
        scope.changePassword=function(){
            usersFactory.changePassword(scope.userPssword).done(function(response){
                var obj =JSON.parse(response) ;
                if(obj.success){
                    scope.userPssword = {};
                    setFormMessage(true,messageTypes.SUCCESS, filter('xlat')('textKeys.alert.changes.saved'),"passwordForm");
                }else{
                    setFormMessage(true,messageTypes.ERROR, filter('xlat')(obj.errorMessage),"passwordForm");
                }
                scope.$apply();
            });
        };


        scope.subMenuItems = [
            {text : filter('xlat')('textKeys.userProfile'), link :"/settings/profile", name:"profile"},
            {text : filter('xlat')('textKeys.webSettings'), link :"/settings/website-settings", name:"website-settings"},
            {text : filter('xlat')('textKeys.language'), link :"/settings/language", name:"language"}
        ];
    }
]);
