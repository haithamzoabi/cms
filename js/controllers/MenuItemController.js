appControllers.controller('MenuItemController', [
    '$scope', '$routeParams', '$http', '$filter', '$modal', '$location', 'mainHelper', 'loadMask',
    function ($scope, $routeParams, $http, $filter, $modal, $location, mainHelper, loadMask) {
        var self = this;
        loadMask.showLoading(null,'MenuItemController');
        $scope.menuItemId = $routeParams.menuItemId;
        self.tab = $routeParams.tab || 'page';
        $scope.postId = $routeParams.postId;
        var postUrl = "server/saveMenuItem.php";
        $scope.menuData = {};
        $scope.errorMessage = $filter('xlat')('textKeys.formNotValid');
        $scope.resetAlerMessage = function () {
            $scope.alertMessage = false;
        };
        $scope.resetAlerMessage();
        $scope.summernoteOptions = {
            height: 300,
            width: '100%',
            minHeight: null,
            maxHeight: null,
            focus: true,
            lang: "he-IL"
        };

        self.titleTextByType = {
            subjects: "textKeys.editSubject",
            posts: "textKeys.editPost"
        };

        var request = mainHelper.getMenuItemData($scope.menuItemId);
        request.success(function (data) {
            $scope.menuData = data[0];
            $scope.menuData.mGalleryCheck = data[0].mGallery.length !== 0;
            updateTabsVisibility( data[0].mViewPages , data[0].mViewPosts );
        });

        var updateTabsVisibility = function(viewPages, viewPosts){
            self.tabs[1].isVisible = viewPages!=='0';
            self.tabs[2].isVisible = viewPosts;
            loadMask.hideLoading('MenuItemController');
        };

        var fixTabsLayout= function(){
            setTimeout(function(){				
                $('.nav-tabs').tabdrop('layout');
            },100);			
        };

        var savedTabs = localStorage.savedTabs ? JSON.parse(localStorage.savedTabs): {};
        if (savedTabs[$scope.menuItemId]){
            for(var i in savedTabs[$scope.menuItemId] ){
                var t = savedTabs[$scope.menuItemId][i];
                savedTabs[$scope.menuItemId][i]["active"] =  ( t.id === ($scope.postId ? "tab-"+$scope.postId : self.tab) );

            }
        }	
        self.tabs = {};
        self.tabs =  savedTabs[$scope.menuItemId] && savedTabs[$scope.menuItemId].length > 0 ? savedTabs[$scope.menuItemId] :  [
            {
                title: $filter('xlat')('textKeys.page'), 
                content: 'pages/menuItems-page.php', 
                href:'/menuItem/'+$scope.menuItemId+'/page',
                active: !self.tab || self.tab==='page',
                isVisible: true,
                id: "page"
            },
            {
                title: $filter('xlat')('textKeys.subjects'),
                content: 'pages/menuItems-subjects.php', 
                href:'/menuItem/'+$scope.menuItemId+'/subjects',
                active: self.tab==='subjects',
                isVisible: false,
                id: "subjects"
            },
            {
                title: $filter('xlat')('textKeys.posts'), 
                content: 'pages/menuItems-posts.php', 
                disabled: true, href:'/menuItem/'+$scope.menuItemId+'/posts',
                active: self.tab==='posts',
                isVisible: false,
                id: "posts"
            }
        ];
        if (savedTabs[$scope.menuItemId]){
            fixTabsLayout();
        }

        $scope.save = function () {
            $.ajax({
                type: "POST",
                url: postUrl,
                data: $.param($scope.menuData),
                success: function (response) {
                    $scope.alertMessage = $filter('xlat')('textKeys.saveSuccess');
                    updateTabsVisibility( $scope.menuData.mViewPages );
                    $scope.$apply();
                },
                dataType: 'json'
            });
        };

        $scope.openFileManager = function (model, selection ,scopeObject) {

            selection = selection?selection:'';
            var scopeObject = scopeObject?scopeObject:$scope['menuData'];
            var instanceModal = $modal.open({
                template: '<filemanager modal="true" listview="false" selection="'+selection+'" ></filemanager>',
                size: 'lg',
                backdrop: true,
                resolve: {}
            });

            instanceModal.result.then(function (result) {
                scopeObject[model] = result;
            });

        };

        var saveTabs = function(){
            savedTabs[$scope.menuItemId] = self.tabs;
            localStorage.savedTabs = JSON.stringify(savedTabs);	
        };

        var findTabIfExist = function(id){
            var currentTabs  = self.tabs;
            for (var i in currentTabs ){
                var tab = currentTabs[i];
                if (tab.id === 'tab-'+id ){
                    return tab;
                }
            }
            return false;
        };

        $scope.addNewTab = function(data){

            var id = String.format("tab-{0}" , data.id  );
            var title = $filter('xlat')('textKeys.newSubject');
            var contentPage =  'pages/subjectForm.php';
            if (data.type === 'posts' ){
                title = $filter('xlat')('textKeys.newPost');
                contentPage =  'pages/postForm.php';
            }		
            if (Number(data.id) ){
                var key = String.format($filter('xlat')(self.titleTextByType[data.type]), data.id );
                title = key;
            }

            var tabHref = String.format("/menuItem/{0}/{1}/{2}", $scope.menuItemId, data.type, data.id );
            if ( $("#"+id).length === 0 ){
                var tabInfo = {
                    title: title, 
                    content: contentPage, 
                    href: tabHref,
                    active: true,
                    isVisible: true,
                    closable: true,
                    id: id
                };
                self.tabs.push(tabInfo);
                fixTabsLayout();
                saveTabs();
            }
            $location.path( tabHref );
        };

        if (Number($scope.postId)){
            if (!findTabIfExist($scope.postId)) {                
                $scope.addNewTab({
                    id: $scope.postId,
                    type: self.tab
                });
            }
        }        

        $scope.onCloseTabClick = function(item,event,index){
            event.preventDefault();
            self.tabs.splice(index,1);
            var jqEl = $("#"+item.id);
            if (jqEl.length> 0 && jqEl.parent().hasClass('dropdown-menu')){				
                jqEl.remove();
            }
            saveTabs();
            fixTabsLayout();
            if (item.active){
                $location.path( String.format("/menuItem/{0}/{1}", $scope.menuItemId, self.tab ) );
            }

        };

        $scope.$on("openNewTab", function(event,data){            			
            $scope.addNewTab(data);
        });

        $scope.$on("closeCurrentTab", function(){
            var currentTab = $(".nav-tabs li.active");
            var tabIndex = currentTab.index(); 
            $(".close",currentTab).click();


        });



    }
]);
