appControllers.controller('PostFormController',[
    '$scope', '$filter', '$http', '$routeParams', 'loadMask',
    function($scope, $filter, $http, $routeParams, loadMask){

        $scope.postId = $routeParams.postId;
        $scope.categoryId = $routeParams.category;
        $scope.errorMessage = '';
        $scope.postData = {
            visible: false
        }; 
        $scope.contentVisible = true;
        $scope.galleryVisbile = true;
        
        var fetchPostData = function(id){
            var loadingText = $filter('xlat')('textKeys.loading');
            loadMask.showLoading(loadingText,'PostFormController');
            var postUrl = 'server/posts.php?id='+id;
            $.ajax({
                type: "GET",
                url: postUrl,
                success: function (response) {
                    if (response.success){
                        $scope.postData = response.data;
                        $scope.postData.type = 1;
                        $scope.postData.visible =  $scope.postData.visible==="1";
                        $scope.$apply();
                    } else { 
                        $scope.errorMessage =  response.errorMessage;
                        $scope.$apply();
                    }
                    loadMask.hideLoading('PostFormController');
                },
                error: function(){
                    $scope.errorMessage = $filter('xlat')("textKeys.COULD_NOT_FETCH_DATA");
                    $scope.$apply();
                    loadMask.hideLoading('PostFormController');
                },
                dataType: 'json'
            })

        };
        if (Number($scope.postId)){
            fetchPostData($scope.postId);
        }


        var addNewPost = function(){

            $scope.postData.menuId = $scope.$parent.menuItemId;
            var postUrl = String.format("server/posts.php?query=new&categoryId={0}" , $scope.categoryId );;
            $.ajax({
                type: "POST",
                url: postUrl,
                data: $.param($scope.postData),
                success: function (response) {
                    if (response.success){
                        $scope.$emit("closeCurrentTab");
                    } else { 
                        $scope.errorMessage = $filter('xlat')( response.errorMessage );
                        $scope.$apply();
                    }
                },
                error: function(){
                    $scope.errorMessage = $filter('xlat')("textKeys.ERROR_ON_SAVE");
                    $scope.$apply();
                },
                dataType: 'json'
            });

        };

        var updatePost = function(){
            $scope.postData.sType = $scope.postData.type.value;
            $scope.postData.menuId = $scope.$parent.menuItemId;
            var postUrl = 'server/posts.php?query=update';
            $.ajax({
                type: "POST",
                url: postUrl,
                data: $.param($scope.postData),
                success: function (response) {
                    if (response.success){
                        $scope.$emit("closeCurrentTab");
                    } else { 
                        $scope.errorMessage = $filter('xlat')( response.errorMessage )||response.errorMessage;
                        $scope.$apply();
                    }
                },
                error: function(){
                    $scope.errorMessage = $filter('xlat')("textKeys.ERROR_ON_SAVE");
                    $scope.$apply();
                },
                dataType: 'json'
            });
        };


        $scope.savePost = function(){
            if ( Number($scope.postId) ){
                updatePost();
            } else if($scope.postId === 'new') {
                addNewPost();                
            }            
        };



    }
]);
