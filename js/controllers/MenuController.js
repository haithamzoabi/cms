appControllers.controller('MenuController', [
	'$scope', '$modal', '$filter', '$location', '$compile', '$http', 'dialogs',
	function (scope, $modal, filter, $location, $compile, $http, dialogs) {

		scope.selectedItems = [];
        var tableDataUrl = 'server/menusData.php';
		var menusTable = $("#menus-table");
		var updateSelectedItems = function () {
			scope.selectedItems = menusTable.bootstrapTable("getSelections");
			scope.$digest();
		};
        var reloadTableData = function(){
            menusTable.bootstrapTable('refresh', {
                url: tableDataUrl
            });
        };
        
        
        var deleteItemAfterConfirm = function () {
			var deleteFailureText = filter('xlat')('textKeys.deleteFailureText');
			var selectedRows = scope.selectedItems;
			var idsToDelete = [];
			for (var i in selectedRows) {
				var row = selectedRows[i];
				idsToDelete.push(row.id);
			}
			
			var request = {
				url: 'server/MenuItem.php',
				method: 'DELETE',
				data: {idsToDelete: idsToDelete}
			};
			$http(request).then(function (response) {
				var data = response.data;             
				if (data.success) {
					reloadTableData();
                    scope.$parent.drawMenu();
				} else {
                    dialogs.error(undefined,deleteFailureText);
                }
			});
		};
        
		scope.menusTableOtions = {
			height: 550,
			onLoadSuccess: function () {
				$compile(menusTable.contents())(scope);
				scope.selectedItems = [];
			},
			onCheck: updateSelectedItems,
			onCheckAll: updateSelectedItems,
			onUncheck: updateSelectedItems,
			onUncheckAll: updateSelectedItems,
			onClickRow: function (row, element) {
				var index = $(element).data('index');
				menusTable = $("#menus-table");
				menusTable.bootstrapTable('uncheckAll');
				menusTable.bootstrapTable('check', index);
				updateSelectedItems();
			},
			url: tableDataUrl,
			striped: true,
			pagination: false,
			pageSize: 50,
			pageList: [10, 25, 50, 100, 200],
			search: true,
			iconSize: 'sm', minimumCountColumns: 2,
			clickToSelect: false,
			selectItemName: 'radioName',
			showColumns: true,
			showRefresh: true,
			showToggle: false,
			columns: [
				{
					field: 'state',
					checkbox: 'true',
				},
				{
					title: '#',
					field: 'id',
					width: '25',
					formatter: function (value, row, index) {
						return index + 1;
					}
				},
				{
					field: 'mTitle',
					title: filter('xlat')('textKeys.title'),
					sortable: true
				},
				{
					field: 'mPosition',
					title: filter('xlat')('textKeys.menuPosition'),
					sortable: true
				},
				{
					field: 'mVisible',
					title: filter('xlat')('textKeys.visbibleMenu'),
					sortable: true,
                    formatter: function(value, row, index){
                        var element = value?'<span class="fa fa-check" ></span>':'<span class="fa fa-times" ></span>';
                        return element;
                    }
				}
			]
		};

		var openDialog = function (params) {
			var instanceModal = $modal.open({
				templateUrl: 'pages/menu-modal.php',
				controller: 'MenuModalController',
				windowTemplateUrl: 'pages/modalWindowTemplte.html',
                windowClass: 'menu-modal-dialog',
				backdrop: false,
				resolve: {
					modalParams: function () {
						return params;
					},
					toAdd: function () {
						return true;
					}
				}
			});
            
            instanceModal.result.then( function(){
                reloadTableData();
                scope.selectedItems= [];
                scope.$parent.drawMenu();
            });
		}

		scope.addMenuItem = function () {
            var modalParams = {
                data: {
                    mVisible: false,
                    mPosition: 0
                },
                toAdd: true
            };
            openDialog(modalParams);
		};
		scope.editMenuItem = function () {
            row = scope.selectedItems[0];
            var modalParams = {
                data: angular.copy(row),
                toAdd: false
            };
            openDialog(modalParams);
		};
		scope.deleteMenuItem = function () {
            var confirmTitle = filter('xlat')('textKeys.warning');
			var deleteEventConfirm = filter('xlat')('textKeys.deleteEventConfirm');
			var confirm = dialogs.confirm(confirmTitle, deleteEventConfirm);
			confirm.result.then(deleteItemAfterConfirm);
		};


	}
]);