appControllers.controller("LoginController", ["$scope", "$http", "$rootScope", "$location", "$filter",
	function ($scope, $http, $rootScope, $location, $filter) {
		$scope.credentials = {};
        
		var authenticate = function(){
            $scope.$parent.authenticate(function () {
                if ($rootScope.authenticated) {
                    $scope.error = false;
                    $location.path("/");
                } else {
                    $scope.error = true;
                }
            });
        };

        $scope.login = function () {

			this.request = {
				url: 'server/CheckLogin.php',
				method: 'POST',
				data: $.param($scope.credentials),
				headers: {
					"content-type": "application/x-www-form-urlencoded"
				}
			};

			var onLoginSuccess = function (data) {
				$scope.errorMessage = $filter('xlat')(data.errorMessage);   
				if (data.success){
					$('form[name="loginForm"]').submit();
				}
			};
			var onLoginError = function () {
				$location.path("/login");
				$scope.error = true;
				$rootScope.authenticated = false;
			};

			$http(this.request).success(onLoginSuccess).error(onLoginError);

		};

	}
]);