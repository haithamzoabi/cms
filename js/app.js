// Loads the correct sidebar on window load,
// collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height-1) + "px");
        }
    });

});


controllerServeices = [
    'xlat',
    'ngTableExport',
    'ui.bootstrap',
    'ngRoute',
    'ui.bootstrap.transition',
    'dialogs.default-translations',
    'dialogs.main',
    'summernote',
    'fileUploaderModule'
];
var isMinified = false;
var scripts = [
    'js/controllers/MainController.js',
    'js/controllers/MenuController.js',
    'js/controllers/LoginController.js',
    'js/controllers/MenuModalController.js',
    'js/controllers/MenuItemController.js',
    'js/controllers/SettingsController.js',
    'js/controllers/SubjectsTableController.js',
    'js/controllers/PostsTableController.js',
    'js/controllers/SubjectFormController.js',
    'js/controllers/PostFormController.js',

    'js/directives/navbarDirective.js',
    'js/directives/angular-summernote.js',
    'js/directives/treeview.js',
    'js/directives/filemanager.js',
    'js/directives/initTable.js',
    'js/directives/rcSubmit.js',
    'js/directives/smallDirectives.js',

    'js/services/xlat.js',
    'js/services/usersFactory.js',
    'js/services/loadMask.js',
    'js/services/mainHelper.js',
    'js/services/fileUploader.js',
    'js/services/WebSettingsFactory.js'
];

var routConfig = function ($routeProvider) {
    $routeProvider.when('/', {

        template: '',
        controller: 'MainController'

    }).when('/menu', {

        templateUrl: 'pages/menu.php',
        controller: 'MenuController'

    }).when('/login', {

        templateUrl: 'pages/login.php',
        controller: 'LoginController'

    }).when('/menuItem/:menuItemId', {

        templateUrl: 'pages/menuItem.php'

    }).when('/menuItem/:menuItemId/:tab', {

        templateUrl: 'pages/menuItem.php',

    }).when('/menuItem/:menuItemId/:tab/:postId', {

        templateUrl: 'pages/menuItem.php',

    }).when('/filemanager', {

        templateUrl: 'pages/filemanager.php'

    }).when('/settings/:profilePage', {

        templateUrl: 'pages/profile-settings.php'

    }).when('/settings/', {

        redirectTo : '/settings/profile'

    }).otherwise({
        //controller: ''
    });
};
var appControllers = angular.module('appControllers', controllerServeices);
var webCms = angular.module('webCms', ['ngRoute', 'appControllers']);
$script(scripts, function() {
    angular.bootstrap(document, ['webCms']);
});
webCms.config(routConfig);    

