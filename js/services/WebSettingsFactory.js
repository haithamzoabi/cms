appControllers.factory('WebSettingsFactory', [
    '$http', function (http) {

        function getSettings(){
            return http.get('server/websiteSettings.php').then(function(response){
                return response.data;;
            });
        };

        function updateSettings(settings){
            settings.update = true;
            return $.ajax({
                type: "POST",
                url: 'server/websiteSettings.php',
                data: settings
            });
        };

        return {
            getSettings : getSettings,
            updateSettings : updateSettings
        };

    }]);
