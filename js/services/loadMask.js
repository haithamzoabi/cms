appControllers.factory('loadMask', [
    '$http', '$filter', 
    function (http, filter) {
        'use strict';
        return {
            loadOverElement : '#page-wrapper',
            loadText: filter('xlat')('textKeys.pleaeWait'),
            isRunning: false,
            from: '',
            showLoading: function(customText,fromPage) {
                this.from = fromPage;
                $(this.loadOverElement).mask(customText ? customText : this.loadText);
                this.isRunning = true;
            },
            hideLoading: function(fromPage) {
                var me = this;
                if (me.from === fromPage){
                    setTimeout(function(){
                        $(me.loadOverElement).unmask();
                        me.isRunning = false;
                        me.from ='';
                    },200);
                }
            }
        };

    }
]);