appControllers.factory('usersFactory', [
'$http', 
function (http) {
    return {
        users: [],
        currentUser: null,
        getUsers : function(){
            return http.get('server/users.php?query=getUserById').then(function(response){
                return response.data;
            });
        },
        getCurrentUserData :function(){
            return http.get('server/users.php?query=getUserById').then(function(response){
                return response.data;
            });
        },
        updateUser :function(userData){
            userData.update = true;
            return $.ajax({
                type: "POST",
                url: 'server/users.php?query=update',
                data: userData
            });
        },
        changePassword: function(userPassword){
            userPassword.update = true;
            return $.ajax({
                type: "POST",
                url: 'server/users.php?query=update',
                data: userPassword
            });
        },
        getUsernameById:  function(id){
            var me = this;
            return http.get('server/users.php?query=getUserById&id='+id).then(function(response){
                me.currentUser = response.data;
                return me.currentUser;
            });
        }
    };
}
]);
