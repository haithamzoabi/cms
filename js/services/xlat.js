/**
 * code from http://blog.trifork.com/2014/04/10/internationalization-with-angularjs/
 * */
var xlat = angular.module('xlat', []);

xlat.factory('xlatService', ['$http','$interpolate',
	function ($http , $interpolate) {
		var currentLanguage = 'he';
		// var tables = $.extend(true, {}, initialXlatTables);
		var tables = null;
		var service = {
			getData: function () {
				var req = {
					method: 'GET',
					url: 'local/he.php',
					cache: true,
					headers: {
						'Content-Type': 'json'
					}
				};
				$http(req).success(function (data) {
					tables = data;
				});
			},
			setCurrentLanguage: function (newCurrentLanguage) {
				currentLanguage = newCurrentLanguage;
			},
			getCurrentLanguage: function () {
				return currentLanguage;
			},
			xlat: function (label, parameters) {
				if (!tables) {
					return "";
				}
				if (parameters === null || $.isEmptyObject(parameters)) {
					return tables[currentLanguage][label];
				} else {
					return $interpolate(tables[currentLanguage][label])(parameters);
				}
			}
		};
		service.getData();
		return service;
	}
]);


xlat.filter('xlat', ['xlatService',
	function (xlatService) {
		function myFilter(label, parameters) {
			return xlatService.xlat(label, parameters);
		}
		;
		myFilter.$stateful = true;
		return myFilter;
	}
]);
xlat.filter('range', function () {
	return function (input, total) {
		total = parseInt(total);
		for (var i = 0; i < total; i++)
			input.push(i);
		return input;
	};
});
