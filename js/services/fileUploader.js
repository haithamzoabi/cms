var fileUploaderModule = angular.module('fileUploaderModule', []);

fileUploaderModule.service('fileUploader', ['$http', function ($http) {
    var self = this;

    self.requesting = false;
    self.upload = function(params, success, error) {
        var form = new FormData(),
            file,fileObj;
        
        form.append('path', params.path );
        form.append('type', 'MultiFileUpload' );
        form.append('ResourceType', 'File' );
        for (file in params.fileList) {
            if (params.fileList.hasOwnProperty(file)){
                fileObj = params.fileList[file];
                //typeof fileObj === 'object' && form.append('file-' + (1 + parseInt(file)), fileObj);
                if( typeof fileObj === 'object') {
                    form.append('filedata[]', fileObj);
                }
            }
        }

        self.requesting = true;
        $http.post('server/filemanager/handleupload.php', form, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(data) {
            self.requesting = false;
            if (typeof params.success === 'function'){
                params.success(data);
            }
        }).error(function(data) {
            self.requesting = false;
            if (typeof params.error === 'function'){
                params.error(data);
            }
        });

    };
}]);