appControllers.factory('mainHelper', [
    '$http', '$filter', 
    function (http, filter) {
        'use strict';
        return {
            pageTitle: 'CMS',
            disableMainLoadMask: false,
            title: function() {
                return this.pageTitle;
            },
            setTitle: function(newTitle){
                return this.pageTitle = newTitle;
            },
            getMenuItemData: function(menuItemId) {
                return http({
                    url: 'server/MenuItem.php',
                    method: 'GET',
                    params: {
                        menuItemId: menuItemId
                    }
                });
            }
        };

    }
]);