appControllers.directive('ngFile', function($parse) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.ngFile),
				modelSetter = model.assign;

			element.bind('change', function() {
				scope.$apply(function() {
					modelSetter(scope, element[0].files);
				});
			});
		}
	};
});

appControllers.directive('toggle', function($parse){
	return {
		restrict: 'C',
		transclude: false,
		replace: false,
		link: function(scope, element, attrs){
			var checkbox = $("input" , element),
				model= checkbox.attr('ng-model'),
				modelGetter = $parse(model),
				modelSetter = modelGetter.assign;
			checkbox.on("change" , function(){
				var val = checkbox.is(':checked') ;
				if (!scope.$$phase){
					scope.$apply(function() {
						modelSetter(scope, val);
					});
				}
			});
			scope.$watch(model,function(newValue, oldValue){
				$('#'+checkbox.attr('id')).bootstrapToggle(newValue?'on':'off');
			})
		}
	}
});


appControllers.directive('autoComplete', function() {
    return function(scope, iElement, iAttrs) {
        iElement.autocomplete({
            source: scope[iAttrs.uiItems],
            select: function () {
                setTimeout(function () {
                    iElement.trigger('input');
                }, 0);
            }
        });
    };
});