appControllers.directive("treeModel", function($compile) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {            
            var treeId = attrs.treeId;
            var treeModel = attrs.treeModel;
            var nodeId = attrs.nodeId || 'id';
            var nodeLabel = attrs.nodeLabel || 'label';
            var nodeChildren = attrs.nodeChildren || 'children';
            var template = String.format('<ul>' +
                '<li data-ng-repeat="node in {0}" >' +
                '<i class="fa fa-folder" data-ng-show="node.{1}.length && node.collapsed" data-ng-click="{2}.selectNodeHead(node)"></i>' +
                '<i class="fa fa-folder-open" data-ng-show="node.{1}.length && !node.collapsed" data-ng-click="{2}.selectNodeHead(node)"></i>' +
                '<i class="fa fa-folder-o" data-ng-hide="node.{1}.length"></i> ' +
                '<span data-ng-class="node.selected" data-ng-click="{2}.selectNodeLabel(node)">{{node.{3}}}</span>' +
                '<div data-ng-hide="node.collapsed" data-tree-id="{2}" data-tree-model="node.{1}" data-node-id={4} data-node-label={3} data-node-children={1}></div>' +
                '</li>' +
                '</ul>',treeModel,nodeChildren,treeId,nodeLabel,nodeId);

            //check tree id, tree model
            if( treeId && treeModel ) {

                //root node
                if( attrs.angularTreeview ) {

                    //create tree object if not exists
                    scope[treeId] = scope[treeId] || {};

                    //if node head clicks,
                    scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function( selectedNode ){

                        //Collapse or Expand
                        selectedNode.collapsed = !selectedNode.collapsed;
                    };

                    //if node label clicks,
                    scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function( selectedNode ){

                        //remove highlight from previous node
                        if( scope[treeId].currentNode && scope[treeId].currentNode.selected ) {
                            scope[treeId].currentNode.selected = undefined;
                        }

                        //set highlight to selected node
                        selectedNode.selected = 'selected';

                        //set currentNode
                        scope[treeId].currentNode = selectedNode;
                    };
                }

                //Rendering template.
                element.html('').append( $compile( template )( scope ) );
            }

        }
    }
});