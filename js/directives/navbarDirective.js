appControllers.controller('navbarController', [
    "$scope", "$rootScope", "$http", 
    function ($scope, $rootScope, $http) {
       /*
        * $scope is same as mainController's scope
        */

       var tableDataUrl = 'server/menusData.php?onlyVisible=true';

       $scope.drawMenu = function(){
           $rootScope.menuItems = $scope.menuItems = [
               {isPublic: false, name: 'textKeys.menu', link: "/menu", icon: "fa-dashboard"},
           ];
           $http.get(tableDataUrl).success(function(data){
               for (var i in data){
                   var item = data[i];
                   $scope.menuItems.push({
                       isPublic: false,
                       name: item.mTitle,
                       displayName: item.mTitle,
                       link: '/menuItem/'+item.id,
                       icon: 'fa-list'
                   });
               }
           });

            setTimeout(function(){
				var isToggleBarsVisible = $(".navbar-toggle").is(":visible");
				if (isToggleBarsVisible){
                	$("#navbar").on("click", "a", null, function () {
                    	$("#navbar").collapse('hide');
                	});     
				}
            },50);

       };

       var hideMenu = function(menuEl){
           $(menuEl).hide();
       }
   }
]);

appControllers.directive('navigation' , function(){
    return {
        restrict:'E',
        replace: true,
        templateUrl: "pages/navbar.php",
        controller: "navbarController",
        link: function(scope, element, attrs, controller) {
            scope.drawMenu();            
        }
    }	
});
