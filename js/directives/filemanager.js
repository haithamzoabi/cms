/*
    filemanager.js 
    Controllers: 
        - fileMangerController
        - FileDetailsController
    FMGENERAL:
        is used as a helper between FM controllers in this file
*/
if (!fileManagerController){
    
    var FMGENERAL = {
        selectedFile: null
    };
    
	var fileManagerController = appControllers.controller('fileManagerController',[
		'$scope', '$filter', '$http', 'fileUploader', 'dialogs', 
		function($scope, filter, $http, fileUploader, dialogs){

			var controller = this,
				filesTable = $("#manager-files-table"),
				$config,updateSelectionPath,FileNavigator,
				getTableFilesDataUrl,reloadTableData,
				deleteFileAfterConfirm,updateSelectedItems;
			
			$scope.showFileInfo = false;

			$scope.managerTreeView = {};
			$scope.treedata = [];
			$scope.viewHeight = 400;
			$scope.fileUploader = fileUploader;
			$scope.selectionPath = '';
			$scope.selection = 'folder';

			$config = {            
				isEditableFilePattern: '\\.(txt|html|htm|aspx|asp|ini|pl|py|md|php|css|js|log|htaccess|htpasswd|json)$',
				isImageFilePattern: '\\.(jpg|jpeg|gif|bmp|png|svg|tiff)$',
				isAudioFilePattern: '\\.(mp3)$',
				isVideoFilePattern: '\\.(swf|mov)$',
				rootPath: "userfiles/",
				basePath: "C:/wamp/www/cms/",
				listUrl: "server/filemanager/get_files.php",
				createFolderUrl: 'server/filemanager/handleupload.php'           
			};

			updateSelectionPath = function(path){
				$scope.selectionPath = path.substr( $config.basePath.length  );
			};

			$scope.toggleView = function(viewPath){
				$scope.filesview = sessionStorage.filesview = viewPath;
                $scope.showFileInfo = false;
                FMGENERAL.selectedFile = null;
			};       

			FileNavigator = function() {
				this.requesting = false;
				this.fileList = [];
				this.currentPath = '';
				this.history = [];
				this.currentDisplayPathArray = [];
				this.error = '';
				this.isEditable = function(name) {
					var val = !!name.toLowerCase().match(new RegExp($config.isEditableFilePattern));
					return val;
				};
				this.isImage = function(name) {
					var val = !!name.toLowerCase().match(new RegExp($config.isImageFilePattern));
					return val;
				};
				this.isAudio = function(name) {
					var val = !!name.toLowerCase().match(new RegExp($config.isAudioFilePattern));
					return val;
				};
				this.isVideo = function(name) {
					var val = !!name.toLowerCase().match(new RegExp($config.isVideoFilePattern));
					return val;
				};
				this.getFileTypeIcon = function(name){
					if ( this.isEditable(name)){
						return 'fa fa-file-text';
					}
					if ( this.isImage(name)){
						return 'fa fa-file-photo-o';
					}
					if ( this.isAudio(name)){
						return 'fa fa-file-sound-o';
					}
					if ( this.isVideo(name)){
						return 'fa fa-file-movie-o';
					}
					return 'fa fa-file';
				};
				this.getDisplayPath = function(){
					var path = this.currentPath,
						displayPath = 'root';

					if ( path.indexOf($config.basePath) !== -1 ){
						displayPath = path.substr( $config.basePath.length  );
					}    
					displayPath = displayPath.replace( $config.rootPath , "root/" );
					this.currentDisplayPathArray = displayPath.split("/");
					return displayPath;
				};
			};

			getTableFilesDataUrl = function(){
				var dir = $scope.fileNavigator.currentPath;
				if($scope.managerTreeView.currentNode){
					dir = $scope.managerTreeView.currentNode.id;
				}            
				return $config.listUrl+"?dir="+dir;
			};
			reloadTableData = function(){
				$("#manager-files-table").bootstrapTable('refresh', {
					url: getTableFilesDataUrl() 
				});
			};

			$scope.fileNavigator = new FileNavigator();

			$scope.fileNavigator.refresh = function(success, error) {
				var self = this,
					path = self.currentPath,
					data = {
						params: {
							dir: path
						}
					};

				self.requesting = true;
				self.fileList = [];
				self.error = '';                
				$http.get($config.listUrl, data).success(function(data) {
					self.fileList = [];
					angular.forEach(data.files, function(file) {
						self.fileList.push(file);
					});
					self.requesting = false;
					if (data.error) {
						self.error = data.error;
						return typeof error === 'function' && error(data);
					}
					return typeof success === 'function' && success(data);
				}).error(function(data) {
					self.requesting = false;
					return typeof error === 'function' && error(data);
				});
			};


			$scope.treeViewData = function(){
				var request = {
					method: 'GET',
					url: 'server/filemanager/get_dir.php',
					type: 'json'
				};
				$http(request).success(function(result){
					$scope.treedata = result;
				});
			};


			$scope.loadFiles = function(path){
				if ($scope.filesview.indexOf("thumb") !== -1){
					$scope.fileNavigator.refresh();
				} else {
					$scope.filesTableOptions.url = getTableFilesDataUrl();
					reloadTableData();
				}
			};


			$scope.$watch( 'managerTreeView.currentNode', function( newObj, oldObj ) {
				if (newObj && newObj.id){
					$scope.fileNavigator.currentPath = sessionStorage.currentPath = newObj.id;

					if ($scope.selection !== 'files' ){
						updateSelectionPath(newObj.id);
					}
					$scope.loadFiles(newObj.id);    
				}
			}, false);

			$scope.smartClick = function(item){
                $scope.showFileInfo = false;
                $scope.selectedItems = [item];
				if ($scope.selection === 'files'){
					updateSelectionPath(item.path);
				}

                setTimeout( function(){
                    $scope.showFileInfo = item.path !== undefined;
                    $scope.$apply();
                } , 0);
                FMGENERAL.selectedFile = item;
			};

			$scope.$watch('filesview', function(newValue, oldValue){
				$scope.loadFiles();
			});


			$scope.openModal = function(item) {
				$scope.temp = {
					name: '',
					path: '',
					inprocess: false
				};
			};

			$scope.createFolder = function(item) {
				var name = item.name && item.name.trim();
				item.type = 'CreateFolder';
				item.ResourceType = 'File';
				item.path = sessionStorage.currentPath;
				if (name) {
					item.inprocess = true;
					item.error = '';
					$.ajax({
						type: "POST",
						url: $config.createFolderUrl,
						data: $.param(item),
						success: function (data) {
							if (data.success){
								$scope.treeViewData();
								$('#newfolder').modal('hide');
							}
							item.inprocess = false;
						},
						error:function(data) {
							item.error = data.result && data.result.error ? data.result.error: filter('xlat')('error_creating_folder');
							item.inprocess = false;
						}
					});

				} else {
					$scope.temp.error = filter('xlat')('error_invalid_filename');
					return false;
				}
			};
			$scope.uploadFiles = function() {
				var params = {
					fileList: $scope.uploadFileList,
					path: $scope.fileNavigator.currentPath,
					success: function() {
						$scope.loadFiles();                        
						$('#uploadfile').modal('hide');
					},
					error: function(){
						console.log("could not upload the file")
					}
				};
				$scope.fileUploader.upload(params);
			};

			deleteFileAfterConfirm = function(){
				var deleteFailureText = filter('xlat')('textKeys.deleteFailureText'),
					selectedRows = $scope.selectedItems,
					idsToDelete = [],i,row,
					request= {};

				for (i in selectedRows) {
					if (selectedRows.hasOwnProperty(i)) {
						row = selectedRows[i];
						idsToDelete.push(row.filename);
					}

				}

				request = {
					url: $config.createFolderUrl,
					method: 'POST',
					data: {
						type: 'DeleteFile',
						path: $scope.fileNavigator.currentPath,
						fileName: idsToDelete[0]
					}
				};
				$.ajax({
					type: "POST",
					url: $config.createFolderUrl,
					data: $.param({
						type: 'DeleteFile',
						path: sessionStorage.currentPath,
						fileName: idsToDelete
					}),
					success: function (data) {
						if (data.success === 'true'){
							reloadTableData();
						} else {
							var error = deleteFailureText+'<br>'+data.error;
							dialogs.error(undefined,error);
						}
					},
					error:function(data) {
						var error = deleteFailureText+'<br>'+data.error;
						dialogs.error(undefined,error);
					}
				});

			};
			$scope.deleteSelectedFiles = function (){
				var confirmTitle = filter('xlat')('textKeys.warning'),
					deleteEventConfirm = filter('xlat')('textKeys.deleteEventConfirm'),
					confirm = dialogs.confirm(confirmTitle, deleteEventConfirm);
				confirm.result.then(deleteFileAfterConfirm);            
			};

			$scope.selectedItems = [];
			updateSelectedItems = function () {
                if(filesTable.length === 0 ){
                    filesTable = $(filesTable.selector);
                }
				FMGENERAL.selectedFile = $scope.selectedItems = filesTable.bootstrapTable("getSelections");
                $scope.showFileInfo = ($scope.selectedItems.length === 1);                 
				$scope.$digest();
			};



			$scope.filesTableOptions = {
				height: $scope.viewHeight,
				onLoadSuccess: function () {
					$compile(filesTable.contents())($scope);
					$scope.selectedItems = [];
				},
				onCheck: updateSelectedItems,
				onCheckAll: updateSelectedItems,
				onUncheck: updateSelectedItems,
				onUncheckAll: updateSelectedItems,
				onClickRow: function (row, element) {
					var index = $(element).data('index');
					filesTable = $("#manager-files-table");
                    var selection = filesTable.bootstrapTable('getSelections');
                    if (selection.length > 0){                        
					   filesTable.bootstrapTable('uncheckAll');
                    }
					filesTable.bootstrapTable('check', index);
					updateSelectedItems();
				},
				responseHandler: function(result){
					return result.files;
				},
				url: getTableFilesDataUrl(),
				striped: true,
				pagination: true,
				pageSize: 50,
				pageList: [10, 25, 50, 100, 200],
				search: true,
				iconSize: 'sm', minimumCountColumns: 2,
				clickToSelect: false,
				selectItemName: 'radioName',
				showColumns: true,
				showRefresh: true,
				showToggle: false,
				columns: [
					{ field: 'state', checkbox: 'true' },
					{ field: 'path', title: '#', width: '25', formatter: function (v, r, i) {return i + 1;} },
					{ field: 'filename', title: filter('xlat')('textKeys.fileName'), sortable: true },
					{ field: 'filesize', title: filter('xlat')('textKeys.fileSize'), sortable: true },
					{ field: 'filedate', title: filter('xlat')('textKeys.date'), sortable: true }
				]
			};
                        
            $scope.toggleShowFileInfo = function(){
                $scope.showFileInfo = !$scope.showFileInfo;
            }
                
        }
	]);

	appControllers.directive('filemanager' , function(){
		return {
			restrict:'E',
			controller: 'fileManagerController',
			controllerAs: 'fm',
			templateUrl: "pages/filemanager/app.php",        
			link: function(scope, element, attrs, controller) {   
                scope.fileDetailsTemplate = 'pages/filemanager/fileDetails.html';
				scope.treeViewData();
				scope.selection = attrs.selection;
				scope.filesview = attrs.listview==='false' ? 'pages/filemanager/filesview-thumb.html' : sessionStorage.filesview || 'pages/filemanager/filesview-list.html';
				scope.isModal = attrs.modal;      
				if (scope.filesview.indexOf("thumb") !== -1){
					scope.fileNavigator.refresh();
				}
				$(".treeview .panel-body").css({maxHeight:scope.viewHeight+"px","overflow":"auto"});
			}
		};	
	});
    
    var FileDetailsController = appControllers.controller('FileDetailsController' , [
        '$scope', '$http',
        function($scope, $http){
            var me = this;
            me.isEditMode  = false;
            me.originalFileData = {};
            me.isAudioFile = false;
            this.viewModeTemplate = 'pages/filemanager/fileDetails-view.html'; 
            this.editModeTemplate = 'pages/filemanager/fileDetails-edit.html'; 
            me.updateCurrentMode = function(isEditMode){
                me.isEditMode = isEditMode;
                me.currentModeTemplate = me[me.isEditMode ?'editModeTemplate':'viewModeTemplate'];
            }; 
            me.updateCurrentMode();
            if (!FMGENERAL.selectedFile || FMGENERAL.selectedFile.length === 0 ){
                $scope.errorResponse = 'textKeys.INCORRECT_FILE_PATH';
                return;
            }
            
            $scope.fileData = {};
            var filePath = me.filePath = FMGENERAL.selectedFile[0]? FMGENERAL.selectedFile[0].path : FMGENERAL.selectedFile.path;
            $scope.selectedFilePath = filePath;
            var lastSlashPos = filePath.lastIndexOf("/")+1;
            var selectedFileName = $scope.fileData.filename = filePath.substr(lastSlashPos);
            me.isAudioFile = (/\.(mp3|mp4)$/i).test(selectedFileName);
            $.ajax({
					type: "GET",
					url: 'server/FileDetails.php',
					data: $.param({
						filePath: filePath
					}),
					success: function (response) {
                        if (response.success){
                            $scope.fileData = response.data;
                            $scope.fileData.filename = selectedFileName;
                            $scope.selectedFilePath = response.data.fileLink;
                            me.originalFileData = angular.copy($scope.fileData);                            
                            $scope.$apply();
                        } else {
                            $scope.errorResponse = response.errorMessage;
                            $scope.$apply();
                        }
					},
					error:function(response) {
                        $scope.errorResponse = response.errorMessage;
                        $scope.$apply();
					}
				});
            
            $scope.cancelAndOpenViewMode = function(){
                me.updateCurrentMode(false);
                $scope.fileData = me.originalFileData;
            };
            
            $scope.saveDetails = function(){ 
                $.ajax({
                    type: "PUT",
                    url: "server/FileDetails.php?filePath="+me.filePath,
                    data: $.param($scope.fileData),
                    success: function (response) {
                        if (response.success){
                            me.updateCurrentMode(false);
                            $scope.fileData = response.data;
                            $scope.fileData.filename = selectedFileName;
                            $scope.errorResponse = false;
                            $scope.$apply();
                        }
                    },
                    error: function(response){
                        console.log("error" , response);
                        //$scope.errorMessage = $filter('xlat')("textKeys.ERROR_ON_SAVE");
                        //$scope.$apply();
                    },
                    dataType: 'json'
                });  
            };
            
            $scope.searchDesc1Url = "server/FileDetails.php?field=desc1";
            $scope.searchDesc2Url = "server/FileDetails.php?field=desc2";
        }
    ]);

}