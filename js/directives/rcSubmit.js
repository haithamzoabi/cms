/**
* code from: http://code.realcrowd.com/on-the-bleeding-edge-advanced-angularjs-form-validation/
* https://github.com/realcrowd/angularjs-utilities
*/
if (!rcSubmitDirective){

    appControllers.controller("rcSubmitController",[
        '$scope','$filter',
        function ($scope, $filter) {
            this.attempted = false;

            var formController = null;

            this.setAttempted = function() {
                this.attempted = true;
            };

            this.setFormController = function(controller) {
                formController = controller;
            };

            this.needsAttention = function (fieldModelController) {
                if (!formController) return false;
                if (fieldModelController) {
                    return fieldModelController.$invalid && (fieldModelController.$dirty || this.attempted);
                } else {
                    if (this.attempted){
                        if (formController.$invalid){
                            $scope.errorMessage = $filter('xlat')('textKeys.formNotValid');
                        } else {
                            $scope.errorMessage = $scope.errorMessage;
                        }
                    }
                    return formController && formController.$invalid && (formController.$dirty || this.attempted);
                }
            };
        }
    ]);

    var rcSubmitDirective = appControllers.directive('rcSubmit',[
        '$parse', 
        function($parse){

            return {
                restrict: 'A',
                scope: false,
                require: ['rcSubmit', '?form'],
                controller: 'rcSubmitController',
                compile: function(cElement, cAttributes, transclude) {
                    return {
                        pre: function(scope, formElement, attributes, controllers) {

                            var submitController = controllers[0];
                            var formController = (controllers.length > 1) ? controllers[1] : null;

                            submitController.setFormController(formController);

                            scope.rc = scope.rc || {};
                            scope.rc[attributes.name] = submitController;
                        },
                        post: function(scope, formElement, attributes, controllers) {

                            var submitController = controllers[0];
                            var formController = (controllers.length > 1) ? controllers[1] : null;
                            var fn = $parse(attributes.rcSubmit);

                            formElement.bind('submit', function (event) {
                                submitController.setAttempted();
                                if (!scope.$$phase) scope.$apply();

                                if (!formController.$valid) return false;

                                scope.$apply(function() {
                                    fn(scope, {$event:event});
                                });
                            });
                        }
                    };
                }
            }	
        }
    ]);
}