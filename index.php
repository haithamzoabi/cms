<!DOCTYPE HTML>
<html lang="en-US"  ng-controller="MainController" >
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="msapplication-tap-highlight" content="no">
        <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>		
        <link href="js/loadMask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>
        <link href="css/green-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/main.min.css" rel="stylesheet" type="text/css"/>
        <link href="js/table/bootstrap-table.min.css" rel="stylesheet" type="text/css"/>
        <link href="js/lib/summernote/summernote.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="js/lib/jui/jquery-ui.min.css">

        <script src="js/lib/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="js/lib/scripts-loader.js" type="text/javascript"></script>
        <script src="js/loadMask/jquery.loadmask.min.js" type="text/javascript"></script>
        <script src="js/lib/date-format.min.js" type="text/javascript"></script>
        <script src="js/lib/common.js" type="text/javascript"></script>
        <script src="js/lib/jui/jquery-ui.min.js" text="text/javascript" ></script>

		<script src="js/lib/angular/angular.min.js" type="text/javascript"></script>
        <script src="js/lib/angular/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="js/lib/angular/angular-route.js" type="text/javascript"></script>

        <script src="js/table/bootstrap-table.min.js" type="text/javascript"></script>
        <script src="js/table/locale/bootstrap-tabls-he-HE.min.js" type="text/javascript"></script>
        <script src="js/lib/summernote/summernote.min.js" type="text/javascript"></script>
        <script src="js/lib/summernote/plugin/summernote-ext-fontstyle.js" type="text/javascript"></script>
        <script src="js/lib/summernote/plugin/summernote-ext-video.js" type="text/javascript"></script>
        <script src="js/lib/summernote/lang/summernote-he-IL.js" type="text/javascript"></script>


        <script src="js/lib/ng-table-export.js" type="text/javascript"></script>
        <script src="js/lib/ui-bootstrap-tpls-0.12.1.min.js" type="text/javascript"></script>
        <script src="js/lib/dialog/dialogs-default-translations.min.js" type="text/javascript"></script>
        <script src="js/lib/dialog/dialogs.min.js" type="text/javascript"></script>

        <link href="js/lib/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="js/lib/bootstrap-toggle/bootstrap-toggle.min.js"></script>
       
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/lib/bootstrap.min.js" type="text/javascript"></script>
        <title ng-bind="Page.title()"></title>
    </head>
    <body dir="rtl" >

        <navigation></navigation>
        <div id="page-wrapper" ng-class="{hiddenMenu: !authenticated}" >
            <div class="container-fluid" ng-view></div>
        </div>
        <footer class="footer" role="contentinfo">
            <div class="container-fluid">
                <p class="text-footer ng-binding"> {{'textKeys.appName'|xlat}} &copy; <?=date('Y')?> </p>
            </div>
        </footer>
    </body>
</html>