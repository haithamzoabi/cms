-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for cms
DROP DATABASE IF EXISTS `cms`;
CREATE DATABASE IF NOT EXISTS `cms` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cms`;


-- Dumping structure for table cms.files
DROP TABLE IF EXISTS `files`;
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `location` tinytext,
  `desc1` text,
  `desc2` text,
  `details` text,
  `tagIds` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table cms.files: ~0 rows (approximately)
DELETE FROM `files`;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;


-- Dumping structure for table cms.menu
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext,
  `position` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT NULL,
  `description` tinytext,
  `content` text,
  `viewContent` int(11) DEFAULT '0',
  `viewPages` int(11) DEFAULT '0',
  `gallery` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table cms.menu: ~7 rows (approximately)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `title`, `position`, `visible`, `description`, `content`, `viewContent`, `viewPages`, `gallery`) VALUES
	(1, 'الرئيسية', 0, 1, 'الصفحة الرئيسية', '<p style="direction: rtl;">ابسيب سيبتسيب איdsadasdaןדsd this is xa<br></p><blockquote>sdfsdfsdfsdfsd fsdf</blockquote>SDF<br><br><br><img src="https://www.google.co.il/images/srpr/logo11w.png" style="width: 231.578125px; height: 81.7840961895911px;"><p></p>', 1, 1, 'userfiles/image/ggggrrtrterert/هيك/أحسن'),
	(2, 'فيديو', 1, 1, 'gfddfg', '<p><br><img class="img-thumbnail" style="width: 50%; line-height: 18.57px;" src="http://localhost/cms/userfiles/image/gal3/laysa.png"><br><br><img class="img-thumbnail" style="width: 25%;" src="userfiles/Man riding a rearing horse.png"><br><br><img class="img-thumbnail" style="width: 25%;" src="userfiles/file/10635813_10152778566851874_6073642397160912357_n.jpg"><br><br><br><br><br><br><br><br></p>', 1, 1, 'userfiles/image/ggggrrtrterert/qwerty'),
	(3, 'صور', 2, 1, 'WERWER', '', 0, 0, ''),
	(4, 'أغاني', 3, 1, 'fddf', 'ZXASDasd', 1, 1, ''),
	(5, 'أخبارنا', 4, 1, 'asd', '', 0, 1, ''),
	(6, 'راسلنا', 5, 1, '', '', 0, 0, ''),
	(7, 'عن الموقع', 6, 1, '', '', 0, 0, '');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Dumping structure for table cms.pages
DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT 'simple content (0) / post (1) / category(2) ',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '0 = main / page id = parent page',
  `title` tinytext,
  `description` tinytext,
  `content` text,
  `image1` tinytext,
  `image2` tinytext,
  `gallery` tinytext COMMENT ' folder in file manager ',
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `visible` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `comments` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Dumping data for table cms.pages: ~11 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `menu_id`, `type`, `parent_id`, `title`, `description`, `content`, `image1`, `image2`, `gallery`, `created`, `updated`, `visible`, `author`, `comments`) VALUES
	(6, 1, 0, 0, 'ببيبيب', 'بيبيبيب', '', '', '', '', '2015-06-06 12:49:49', '2015-06-06 12:49:49', 0, 1, ''),
	(11, 1, 1, 0, 'لا يرد القضاء إلا الدعاء', 'اللهم أنت السلام ومنك السلام تباركت يا ذا الجلال والإكرام', '&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;1- اللهم أنت السلام ومنك السلام تباركت يا ذا الجلال والإكرام .&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;2- اللهم إني أسألك يا الله بأنك الواحد الأحد الفرد الصمد الذي لم يلد ولم يولد ولم يكن له كفوا احد أن تغفر ذنوبي انك أنت الغفور الرحيم.&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;3- اللهم إني أعوذ بك من الكسل والهرم والمأثم والمغرم ومن فتنة القبر وعذاب القبر ومن فتنة النار وعذاب النار ومن شر فتنة الغنى وأعوذ بك من فتنة الفقر وأعوذ بك من فتنة المسيح الدجال اللهم اغسل عني خطاياي بالماء والثلج والبرد ونق قلبي من الخطايا كما ينقى الثوب الأبيض من الدنس وباعد بيني وبين خطاياي كما باعدت بين المشرق والمغرب . متفق عليه عن عائشة&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;4- اللهم أصلح لي ديني الذي هو عصمة أمري وأصلح لي دنياي التي فيها معاشي وأصلح لي آخرتي التي فيها معادي واجعل الحياة زيادة لي في كل خير واجعل الموت راحة لي من كل شر . رواه مسلم عن ابي هريرة&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;5- اللهم إني أعوذ بك من العجز والكسل والجبن والبخل والهرم وعذاب القبر وفتنة الدجال اللهم آت نفسي تقواها وزكها أنت خير من زكاها أنت وليها ومولاها اللهم إني أعوذ بك من علم لا ينفع ومن قلب لا يخشع ومن نفس لا تشبع ومن دعوة لا يستجاب لها . رواه مسلم عن زيد بن الارقم&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;6-اللهم أكثر مالي وولدي وبارك لي فيما أعطيتني واطل حياتي على طاعتك وأحسن عملي واغفر لي.&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;7- لا إله إلا الله العظيم الحليم لا إله إلا الله رب العرش العظيم لا إله إلا الله رب السموات السبع ورب الأرض ورب العرش الكريم . متفق عليه عن ابن عباس&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;8- اللهم رحمتك أرجو فلا تكلني إلى نفسي طرفة عين وأصلح لي شأني كله لا إله إلا أنت . رواه أحمد وابي داود عن أبي بكرة&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;9-اللهم مصرف القلوب صرف قلوبنا على طاعتك&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;10- رب أعني ولا تعن علي وانصرني ولا تنصر علي وامكر لي ولا تمكر علي واهدني ويسر هداي إلي وانصرني على من بغى علي ، اللهم اجعلني لك شاكرا لك ذاكرا لك راهبا لك مطواعا إليك مخبتا إليك أواها منيبا ، رب تقبل توبتي واغسل حوبتي وأجب دعوتي وثبت حجتي واهد قلبي وسدد لساني واسلل سخيمة قلبي . رواه أحمد عن ابن عباس&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;11- اللهم انك عفو تحب العفو فاعف عني . رواه الترمذي عن عائشة&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;12- قل اللهم إني أسألك فعل الخيرات وترك المنكرات وحب المساكين وأن تغفر لي وترحمني وإذا أردت فتنة قوم فتوفني غير مفتون أسألك حبك وحب من يحبك وحب عمل يقرب إلى حبك . رواه الترمذي عن معاذ بن جبل&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;13- اللهم إني أسألك من الخير كله عاجله وآجله ما علمت منه وما لم أعلم وأعوذ بك من الشر كله عاجله وآجله ما علمت منه وما لم أعلم اللهم إني أسألك من خير ما سألك به عبدك ونبيك وأعوذ بك من شر ما عاذ به عبدك ونبيك اللهم إني أسألك الجنة وما قرب إليها من قول أو عمل وأعوذ بك من النار وما قرب إليها من قول أو عمل وأسألك أن تجعل كل قضاء قضيته لي خيرا . رواه ابن ماجه عن عائشة&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;14- اللهم إني ظلمت نفسي ظلما كثيرا وإنه لا يغفر الذنوب إلا أنت فاغفر لي مغفرة من عندك وارحمني إنك أنت الغفور الرحيم . متفق عليه عن ابن عمر وابي بكر&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;15- اللهم إني أسألك موجبات رحمتك وعزائم مغفرتك والسلامة من كل إثم والغنيمة من كل بر والفوز بالجنة والنجاة من النار . رواه الحاكم أبو عبد الله وقال : حديث صحيح على شرط مسلم&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;16- اللهم إني أعوذ بك من التردي والهدم والغرق والحريق وأعوذ بك أن يتخبطني الشيطان عند الموت وأعوذ بك أن أموت في سبيلك مدبرا ، اللهم إني أعوذ بك من العجز والكسل والجبن والبخل والهرم والقسوة والغفلة والعيلة والذلة والمسكنة وأعوذ بك من الفقر والكفر والفسوق والشقاق والنفاق والسمعة والرياء وأعوذ بك من الصمم والبكم والجنون والجذام والبرص وسيء الأسقام . رواه الحاكم عن انس&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;17- اللهم إني أعوذ بك من يوم السوء ومن ليلة السوء ومن ساعة السوء ومن صاحب السوء ومن جار السوء في دار المقامة . رواه الطبراني عن عقبة بن عامر&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;18- اللهم فقهني في الدين .&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;19- اللهم اني أسألك بأن لك الحمد لا إله إلا أنت المنان بديع السماوات والأرض يا ذا الجلال والإكرام يا حي يا قيوم إني أسألك الجنة وأعوذ بك من النار . رواه النسائي عن انس&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;20- اللهم بعلمك الغيب وقدرتك على الخلق أحيني ما علمت الحياة خيرا لي وتوفني إذا علمت الوفاة خيرا لي اللهم وأسألك خشيتك في الغيب والشهادة وأسألك كلمة الإخلاص في الرضا والغضب وأسألك القصد في الفقر والغنى وأسألك نعيما لا ينفد وأسألك قرة عين لا تنقطع وأسألك الرضا بالقضاء وأسألك برد العيش بعد الموت وأسألك لذة النظر إلى وجهك والشوق إلى لقائك في غير ضراء مضرة ولا فتنة مضلة اللهم زينا بزينة الإيمان واجعلنا هداة مهتدين . رواه النسائي عن عمار&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;21- اللهم رب السماوات ورب الأرض ورب العرش العظيم ربنا ورب كل شيء فالق الحب والنوى ومنزل التوراة والإنجيل والفرقان أعوذ بك من شر كل ذي شر أنت آخذ بناصيته اللهم أنت الأول فليس قبلك شيء وأنت الآخر فليس بعدك شيء وأنت الظاهر فليس فوقك شيء وأنت الباطن فليس دونك شيء اقض عنا الدين وأغننا من الفقر . الكلم الطيب عن ابي هريرة&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;22- اللهم ألف بين قلوبنا وأصلح ذات بيننا واهدنا سبل السلام ونجنا من الظلمات إلى النور وجنبنا الفواحش ما ظهر منها وما بطن وبارك لنا في أسماعنا وأبصارنا وقلوبنا وأزواجنا وذرياتنا وتب علينا إنك أنت التواب الرحيم واجعلنا شاكرين لنعمتك مثنين بها قابلين لها وأتمها علينا . رواه ابي داود عن عبدالله&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;23- اللهم جنبني منكرات الأخلاق والأهواء والأدواء . إسناده صحيح وقد رواه اصحاب السنن وغيرهم&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;24- اللهم إني أسألك إيمانا لا يرتد ونعيما لا ينفد ومرافقة محمد في أعلى جنة الخلد . السلسلة الصحيحة 2301&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;25- اللهم إني أعوذ بك من غلبة الدين وغلبة العدو وشماتة الأعداء . رواه النسائي عن ابن عمرو&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;26- اللهم لك الحمد كله اللهم لا قابض لما بسطت ولا باسط لما قبضت ولا هادي لما أضللت ولا مضل لمن هديت ولا معطي لما منعت ولا مانع لما أعطيت ولا مقرب لما باعدت ولا مباعد لما قربت اللهم ابسط علينا من بركاتك ورحمتك وفضلك ورزقك اللهم إني أسألك النعيم المقيم الذي لا يحول ولا يزول اللهم إني أسألك النعيم يوم العيلة والأمن يوم الخوف اللهم إني عائذ بك من شر ما أعطيتنا وشر ما منعت اللهم حبب إلينا الإيمان وزينه في قلوبنا وكره إلينا الكفر والفسوق والعصيان واجعلنا من الراشدين اللهم توفنا مسلمين وأحينا مسلمين وألحقنا بالصالحين غير خزايا ولا مفتونين اللهم قاتل الكفرة الذين يكذبون رسلك ويصدون عن سبيلك واجعل عليهم رجزك وعذابك اللهم قاتل الكفرة الذين أوتوا الكتاب إله الحق.&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;27-اللهم اغفر لنا ولوالدينا وارحمهما والمسلمين والمسلمات الأحياء منهم والأموات الذين شهدوا لك بالوحدانية ولنبيك بالرسالة وماتوا على ذلك وباعد بينهم وبين خطاياهم كما باعدت بين المشرق والمغرب افسح لهم في قبورهم مدا أبصارهم واجعلها لهم روضه من رياض الجنة.&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;28-اللهم اغفر للمسلمين والمسلمات والمؤمنين والمؤمنات الأحياء منهم والأموات انك يا ربنا سميع قريب مجيب الدعوات.&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;29-اللهم صلي على محمد وعلى آل محمد كما صليت على إبراهيم وعلى آل إبراهيم انك حميد مجيد وبارك على محمد وعلى آل محمد كما باركت على إبراهيم وعلى آل إبراهيم انك حميد مجيد&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;وآخر دعوانا أن الحمدلله رب العالمين وصلى الله على نبينا محمد وعلى آله وصحبه اجمعين&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;&lt;br&gt;&lt;/span&gt;&lt;/div&gt;&lt;div&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;بامكانك تحميل المطوية بضيغة Pdf من هنا&lt;/span&gt;&lt;/div&gt;', 'userfiles/image/ggggrrtrterert/qwerty/notepad-screenshot-doaa(1).txt', '', '', '2015-06-06 18:32:41', '2015-12-04 20:07:36', 0, 1, ''),
	(12, 1, 0, 0, 'شبيبئءؤ شسيب', 'للسيبسئيؤ ءؤثقبسءيؤبئ ي ي', '', '', '', '', '2015-06-06 18:34:47', '2015-06-06 18:34:47', 0, 1, ''),
	(13, 1, 0, 0, 'ؤربءؤسيبسيب', 'س يبسيبسيبسيب سيب يس', '', '', '', '', '2015-06-06 18:35:23', '2015-06-06 18:35:23', 0, 1, ''),
	(15, 1, 0, 0, 'سسسيبيب سيبسيب', 'سي بسفصثف ثفليبل يبليبليلب', '', '', '', '', '2015-06-06 18:49:28', '2015-06-06 18:49:28', 0, 1, ''),
	(16, 1, 0, 0, 'ب33', 'بب2س', '', '', '', '', '2015-06-12 16:01:21', '2015-06-12 16:01:21', 0, 1, ''),
	(17, 1, 1, 0, 'ع', '555', '<span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;<br></span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;">ءؤرءرصثق صثسي بسيب سيب&nbsp;</span><span style="line-height: 18.5714282989502px;"><br></span>', 'userfiles/empty text file.txt', 'userfiles/Man riding a rearing horse.png', 'userfiles/image/gal2', '2015-06-12 16:02:07', '2015-06-12 16:02:07', 1, 1, ''),
	(18, 2, 2, 0, 'أعراس', 'أعراس يبوي يخوي', '', '', '', '', '2015-08-01 11:40:11', '2015-12-04 21:59:16', 1, 1, ''),
	(20, 2, 1, 18, 'this is a new post for a3ras videos', 'and the is the description', 'nothing to say more...', 'userfiles/file/10635813_10152778566851874_6073642397160912357_n.jpg', '', '', '2015-10-30 17:23:43', '2015-12-01 20:21:56', 1, 1, ''),
	(21, 2, 1, 18, 'inside a3ras 2', 'inside a3ras 2', 'inside a3ras 2', 'sdfsd33', 't546', '', '2015-10-30 17:38:47', '2015-12-04 22:10:19', 0, 1, ''),
	(22, 2, 1, 18, 'inside a3ras 33', '3inside a3ras 33', 'inside a3ras 33', 'userfiles/Man riding a rearing horse.png', '', '', '2015-10-30 17:39:47', '2015-12-01 19:23:17', 1, 1, '');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


-- Dumping structure for table cms.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` tinytext NOT NULL,
  `u_password` tinytext NOT NULL,
  `u_lastLogin` datetime NOT NULL,
  `email` tinytext,
  `name` tinytext,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Dumping data for table cms.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`u_id`, `u_name`, `u_password`, `u_lastLogin`, `email`, `name`) VALUES
	(1, 'admin', 'admin', '2015-12-05 18:53:08', 'haithamzoabi@gmail.com', 'Haitham Zoabi');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
