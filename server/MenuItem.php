<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

class MenuItem {

    public $db;
    public $menuItemId;
    public function __construct($menuItemId) {
        $this->db = new Database();
        $this->menuItemId = $menuItemId;
    }

    public function fetchMenuItemData(){
        ////id,title,position,visible,description,content,viewContent,viewPages,gallery
        $query = "select * from menu where id='$this->menuItemId'";
        $result = $this->db->query($query);
        $dataArray = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)){
            $object = array(
                'id' => $row["id"],
                'mTitle' => $row["title"],
                'mPosition' => $row["position"],
                'mVisible' => $row["visible"],
                'mDescription' => $row["description"],
                'mContent' => $row["content"],
                'mViewContent' => $row["viewContent"],
                'mViewPages' => $row["viewPages"],
                'mGallery' => $row["gallery"],
                'mViewPosts' => $this->hasPagesWithCategories()
            );
            array_push($dataArray, $object);
        }
        return $dataArray;
    }


    public function getData(){		
        return $this->fetchMenuItemData();
    }

    public function returnResult($result , $message) {
        if ($result) {
            $data = array('success' => true, 'errorMessage' => $message);
        } else {
            $data = array('success' => false, 'errorMessage' => $message);
        }
        echo json_encode($data);
    }

    public function deleteRows($idsArray){
        $ids = implode(",", $idsArray);
        $query = "delete from menu where id in ($ids) ";
        $result =  $this->db->query($query);
        $message = $result ? "deleted":$this->db->getError();
        $this->returnResult($result,$message);
    }
    
    private function hasPagesWithCategories(){
        $total = 0;
        $query = "select count(*) as total from pages where menu_id='$this->menuItemId' and type='2'";
        $result = $this->db->query($query);
        $row = $result->fetch_row();
        $total = $row[0];
        return $total > 0;
    }
    

}

$inputData = json_decode(file_get_contents("php://input"));
require 'Database.php';    

if ($_GET['menuItemId']){
    
    $item = new MenuItem($_GET['menuItemId']);
    echo json_encode($item->getData());
    $item->db->close();
    
} else if (count($inputData->idsToDelete) > 0 ){    
       
    $item = new MenuItem($_GET['menuItemId']);
    $item->deleteRows($inputData->idsToDelete);
    $item->db->close();
}

?>