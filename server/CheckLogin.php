<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
date_default_timezone_set('Asia/Jerusalem');
require 'Database.php';

class CheckLogin {

	public $db;

	public function __construct($post) {
		$data = array();
		if (!array_key_exists('email', $post) && !array_key_exists('password', $post)) {
			$data = array('success' => false, 'errorMessage' => 'error.bad.credentials');
		}
		$this->db = new Database();
		$this->post = $post;
		$this->useCookie = $post['rememberme'];
		$this->eventId  = -1;
		$data = $this->checkAdminLogin();
		echo json_encode($data);
		//$headerParams =  http_build_query($data);
		//header("Location: /cms/#/login/?".$headerParams);
	}

	public function checkAdminLogin() {
		$POSTusername = $this->post['username'];
		$POSTemail = $this->post['email'];
		$POSTpassword = $this->post['password'];
		$POSTremember = $this->useCookie;
		$query = "SELECT * FROM users WHERE email='$POSTemail' ";
		$result = $this->db->query($query);
		if ($result->num_rows !== 0) {
			$row = $result->fetch_assoc();
			$this->userName = $row['u_name'];
			$this->email = $row['email'];
			$this->passWord = $row['u_password'];
			$this->userId = $row['u_id'];
			if ($POSTemail == $this->email && $POSTpassword == $this->passWord) {
				$data = $this->saveCredentials($POSTremember);
			} else {
				$data = array('success' => false, 'errorMessage' => 'error.bad.password' );
			}
		} else {
			$data = array('success' => false, 'errorMessage' => 'error.bad.email');
		}
		return $data;
	}
	

	public function saveCredentials() {
		$_SESSION ['loginId'] = $this->userId;
		$today = date("Y-m-d H:i:s");  
		$updateLastLogin = "update users set u_lastLogin ='$today' where u_id='$this->userId' ";
		$result =  $this->db->query($updateLastLogin);		
		return array('success' => true, 'errorMessage' => '', 'result' => $_SESSION);
	}

}

return new CheckLogin($_POST);
?>
