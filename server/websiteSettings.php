<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require 'Database.php';

class websiteSettings {
    public $db;
    function __construct($session, $get , $post) {
        $this->db = new Database();
        if($post["update"]){
            $data = $this->updateWebsiteSettings($post);
        }else{
            $data = $this->getWebsiteSettings();
        }
        echo json_encode($data);
    }

    public function getWebsiteSettings() {
        $query = "SELECT * FROM websettings";
        $result = $this->db->query($query);
        if ($result->num_rows !== 0) {
            $row = $result->fetch_assoc();
            $object = array(
                'name' => $row["name"],
                'link' => $row["link"],
                'language' => $row["language"],
                'analysisCode' => $row["analysisCode"]
            );
            $data =array('success' => true, 'data' => $object);
        } else {
            $data = array('success' => false, 'data' => null);
        }
        return $data;
    }

    public function updateWebsiteSettings($post) {
        $settings = $this->getWebsiteSettings();

        if (sizeof($settings["data"]) > 0) {
            $query = "UPDATE websettings SET name='".$post["name"]."', analysisCode='".$post["analysisCode"]."', link='".$post["link"]."', language='".$post["language"]."'" ;
        }else{
            $query = "INSERT INTO websettings (name, link, language, analysisCode) VALUES ('".$post["name"]."', '".$post["link"]."', '".$post["language"]."', '".$post["analysisCode"]."')";
        }
        $result = $this->db->query($query);
        return $this->getWebsiteSettings();
    }

}

$userData = new websiteSettings($_SESSION, $_GET , $_POST);
?>
