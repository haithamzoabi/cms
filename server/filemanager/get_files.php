<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
$realpath  = realpath('C:/wamp/www/cms/');
$host_dir = $realpath."/userfiles/";

$dir = $_GET['dir'] . "/";
$dir = $dir==='/'?$host_dir:$dir;

$dRoot = $_SERVER['DOCUMENT_ROOT'];
$dRootLength = strlen($dRoot);
$dh = opendir($dir);
$files = array();


while (($file = readdir($dh)) !== false) {
    if ($file != '.' AND $file != '..' ) {
        if (filetype($dir . $file) == 'file') {
            $files[] = array(
                'filename' => iconv("WINDOWS-1255", "UTF-8", $file),
                'filesize' => filesize($dir . $file). ' bytes',
                'filedate' => date("F d Y H:i:s", filemtime($dir . $file)),
                'path' => substr($dir . $file,0 ),
                'type'=> pathinfo($dir . $file, PATHINFO_EXTENSION)
            );
        }            
    }
}
closedir($dh);

echo(json_encode(array('documentRoot'=>$dRoot,'files' => $files)));
?>