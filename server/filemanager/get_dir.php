<?php
header('Content-Type: application/json;charset=utf-8');
error_reporting(E_ERROR | E_WARNING | E_PARSE);
class MyDirectory {
    
    public $host_dir;
    public $json;

    public function __construct($host_dir) {
        $this->host_dir = $host_dir;
        $this->json = '[{"text": "root","id": "'.str_replace("\\","/",$host_dir).'","collapsed": false,"children": [';
    }
    
    public function get_children($dir, $child) {
        $dh = opendir($dir);
        while (($file = readdir($dh)) !== false) {
            if ($file != '.' AND $file != '..' ) {
                if (filetype($dir . $file) == 'dir') {
                    // must be checked if this folder have other subfolder
                    if ($this->count_sub_dir($dir . $file . '/') == 0) {                        
                        $this->json .= '{"text":"'.$file.'", "leaf": true, "id": "'.str_replace("\\","/",$dir) . $file.'"},';
                    } else {
                        $this->json .= '{"text":"'.$file.'", "id": "'.str_replace("\\","/",$dir) . $file.'", "collapsed":true,"children": [';
                        $this->get_children($dir . $file . '/', true);
                    }
                }
            }
        }
        if ($child) {
            $this->json .=  ']},';
        }
        closedir($dh);
    }
    
    public function count_sub_dir($dir) {
        $dh = opendir($dir);
        $countdir = 0;
        while (($file = readdir($dh)) !== false) {
            if ($file != '.' AND $file != '..' ) {
                if (filetype($dir . $file) == 'dir') {
                    $countdir++;
                }            
            }
        }
        closedir($dh);
        return $countdir;
    }
}
$realpath  = realpath('C:/wamp/www/cms/');
$host_dir = $realpath."/userfiles/";

$dir = new MyDirectory($host_dir);

$dir->get_children($host_dir, false);

$dir->json .= ']}]';
$dir->json = str_replace(",]", "]", $dir->json);

echo($dir->json);