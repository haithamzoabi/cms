<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require 'Database.php';

class menusData {

	public $db;

	public function __construct($get, $post ,$session) {
		$this->db = new Database();
		$this->getData = $get;
		$this->postData = $post;		
		$query = "SELECT * from menu";
        if ( $this->getData['onlyVisible'] === "true" ){
            $query .= " where visible='1' ";
        }
		$result = $this->db->query($query);
		$data = $this->returnDataArray($result);
		echo json_encode($data);
		$this->db->close();
	}

	public function returnDataArray($result) {
		//id,title,position,visible,description,viewContent,gallery
		$dataArray = array();
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$object = array(
				'id' => $row["id"],
				'mTitle' => $row["title"],
				'mPosition' => $row["position"]*1,
				'mVisible' => !!$row["visible"]
			);
			array_push($dataArray, $object);
		}
		return $dataArray;
	}
	

}

return new menusData($_GET, $_POST , $_SESSION);
