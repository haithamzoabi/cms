<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require_once ('Database.php');

class users {
    
    public $db;
    public $session;
    
    public function __construct($session, $get , $post) {
        $this->session = $session;
        $loginId = $session['loginId'];
        $this->db = new Database();
        $query = $get['query'];
        
        if ($query==='update') {
            if($post["update"]) {
                if($post["oldPassword"]) {
                    $data = $this->updatePassword($loginId, $post);
                } else {
                    $data = $this->updateUser($loginId, $post);
                }
                echo json_encode($data);
            }
        }
        
        if ($query==='getUserById'){
            $requestId = $get['id'];
            if ( !$requestId ){
                $requestId = $loginId ;
            }
            $data = $this->getUserById($requestId);
            echo json_encode($data);
        }        


    }

    public function getUserById($id) {
        $query = "SELECT * FROM users WHERE u_id='$id' ";
        $result = $this->db->query($query);
        if ($result->num_rows !== 0) {
            $row = $result->fetch_assoc();
            $object = array(
                'userName' => $row["u_name"],
                'lastLogin' => $row["u_lastLogin"],
                'name' => $row["name"],
                'email' => $row["email"],
                'loginId' => $row["u_id"]
            );
            $data =array('success' => true, 'data' => $object);
        } else {
            $data = array('success' => false, 'errorMessage' => 'error.bad.username');
        }
        return $data;
    }

    public function updateUser($loginId,$post) {
        $query = "UPDATE users SET name='".$post["name"]."', u_name='".$post["userName"]."', email='".$post["email"]."' WHERE u_id='$loginId' ";
        $result = $this->db->query($query);
        return $this->getUserById($loginId);
    }

    public function updatePassword($loginId,$post) {
        $data = $this->getUserById($loginId);
        $query = "SELECT u_password FROM users WHERE u_id='$loginId' ";
        $result = $this->db->query($query);
        if ($result->num_rows !== 0) {
            $row = $result->fetch_assoc();
            if($row["u_password"] == $post["oldPassword"]){
                $query = "UPDATE users SET u_password='".$post["newPassword"]."' WHERE u_id='$loginId' ";
                $result = $this->db->query($query);
                $data = array('success' => true);
            }else{
                $data = array('success' => false, 'errorMessage' => 'error.bad.password');
            }
        }
        return $data;
    }

}

$userData = new users($_SESSION, $_GET , $_POST);
?>
