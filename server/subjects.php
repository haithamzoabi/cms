<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();

class subjects {

    public $db;
    public $subjectId;
    public $userId;
    public $today;
    public $post;
    public function __construct($subjectId,$GET,$POST) {
        $this->db = new Database();
        $this->subjectId = $subjectId;
        $this->today = $today = date("Y-m-d H:i:s"); 
        $this->userId = $_SESSION ['loginId'];
        $this->menuId = $GET['menuId'];
        $this->post = $POST;
    }

    public function fetchMenuItemData(){
        //id,menu_id,`type`,parent_id,title,description,content,image1,image2,gallery,created,updated,visible,author,comments
        $query = "select * from pages where parent_id='0' ";
        if ($this->menuId){
            $query .= " and  menu_id='$this->menuId' ";
        }        
        $isSingle = false;
        if ($this->subjectId && $this->subjectId!=-1 ){
            $query .= " and id='$this->subjectId' ";
            $isSingle = true;
        }
        $result = $this->db->query($query);
        $dataArray = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)){
            $object = array(
                'id' => $row["id"],
                'title' => $row["title"],
                'type' => $row["type"],
                'created' => $this->formatDate($row["created"]) ,
                'updated' => ($row["updated"]===$row["created"]) ? null: $this->formatDate($row["updated"]),
                'visible' => $row['visible'],
                'author'  => $this->getUserName($row['author'])
            );
            if ($isSingle === true){
                $object = array_merge( $object , 
                                      array(
                                          'description' => $row["description"],
                                          'content' => html_entity_decode($row["content"]),
                                          'image1' => $row["image1"],
                                          'image2' => $row["image2"],
                                          'gallery' => $row["gallery"]
                                      ));
            }            
            array_push($dataArray, $object);
        }
        return $dataArray;
    }


    public function getData(){	
        return $this->fetchMenuItemData();
    }

    public function saveNew(){

        $query  = sprintf(" insert into pages (menu_id,`type`,parent_id,title,description,content,image1,image2,gallery,created,updated,visible,author,comments) values ("
                          . "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s' ) ",
                          $this->post['menuId'],
                          $this->post['sType'],
                          0,
                          $this->post['title'],
                          htmlentities($this->post['description'], ENT_COMPAT,'UTF-8', true),
                          htmlentities($this->post['content'], ENT_QUOTES,'UTF-8', true ),
                          $this->post['image1'],
                          $this->post['image2'],
                          $this->post['gallery'],
                          $this->today,
                          $this->today,
                          $this->post['visible']==='true'?1:0,
                          $this->userId,
                          NULL
                         );
        $result = $this->db->query($query);
        $message = $result ? "":'textKeys.FAILED_TO_SAVE';
        return $this->returnResult($result,$message,array());

    }


    public function update(){

        $query = sprintf("update pages set `type`='%s',title='%s',description='%s',content='%s',image1='%s',image2='%s',gallery='%s',updated='%s',visible='%s'"
                         ." where id='%s' ",
                         $this->post['sType'],
                         $this->post['title'],
                         htmlentities($this->post['description'], ENT_COMPAT,'UTF-8', true),
                         htmlentities($this->post['content'], ENT_QUOTES,'UTF-8', true ),
                         $this->post['image1'],
                         $this->post['image2'],
                         $this->post['gallery'],
                         $this->today,
                         $this->post['visible']==='true'?1:0,
                         $this->post['id']
                        );
        $result = $this->db->query($query);
        $message = $result ? "" : $this->db->getError();
        return $this->returnResult($result,$message,array());

    }

    public function returnResult($result, $message , $result) {
        $data = array('success' => empty($message), 'errorMessage' => $message);
        if (!empty($result)){
            $data = array_merge($data, array("data"=>$result));
        }
        return json_encode($data);
    }


    public function formatDate ( $dateString ){
        return date("Y/m/d", strtotime($dateString) );
    } 

    public function getUserName ($userId){
        require_once("users.php");
        $emptyArray= array();
        $userData = new users($emptyArray , $emptyArray ,$emptyArray);
        $user = $userData->getUserById($userId);
        if ($user){
            return $user["data"]["name"];
        }
    }

    public function deleteRows($idsArray){
        $result= false;
        if (count($idsArray)>0){
            $ids = implode(",", $idsArray);
            $query = "delete from pages where id in ($ids) ";
            $result =  $this->db->query($query);
        }
        $message = $result ?"":$this->db->getError();
        return $this->returnResult($result,$message,array());
    }


}
$inputData = json_decode(file_get_contents("php://input"));

if ($_GET['query']=='all'){
    require 'Database.php';
    $subjects = new subjects(-1,$_GET,$_POST);
    echo json_encode($subjects->getData());
}


if ($_GET['query'] == 'new'){
    require_once ('Database.php');
    $subjects = new subjects(-1,$_GET,$_POST);
    echo ( $subjects->saveNew() );
}

if ($_GET['query'] == 'update'){
    require_once ('Database.php');
    $subjects = new subjects(-1,$_GET,$_POST);
    echo ( $subjects->update() );
}

if ($_GET['id']){
    header('Content-Type: application/json');
    require_once ('Database.php');
    $subjects = new subjects($_GET['id'],$_GET,$_POST);
    $response = $subjects->getData();
    echo $subjects->returnResult(count($response)===1,"",$response[0]);
}

if ( $_SERVER['REQUEST_METHOD']=='DELETE' ){
    header('Content-Type: application/json');
    require_once ('Database.php');
    $subjects = new subjects(-1,$_GET,$_POST);
    $response = $subjects->deleteRows($inputData->idsToDelete);
    echo $response;
}


?>
