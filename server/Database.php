<?php

class Database {

    public $connection;
    
    public function __construct() {
        
        $mysql_host = "localhost";
        $mysql_database = "cms";
        $mysql_user = "root";
        $mysql_password = "";
        
        $connection = $this->connection = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_database);
        $connection->query("SET NAMES 'utf8'");
    }

    public function query($sql) {
        $res = $this->connection->query($sql);
        if ($res) {
            return $res;
        } else {
			return false;
        }
    }

	public function getError(){
		return ('SQL Error: ' . $this->connection->error);
	}
	
    public function __set($name, $value) {
        $this->set($name, $value);
    }

    public function __get($name) {
        return $this->get($name);
    }

    public function __call($name, $arguments) {
        echo $name . ' function has been called with parameters ' . implode(',', $arguments);
    }
    
    public function close(){
        $this->connection->close();
    }

}

?>