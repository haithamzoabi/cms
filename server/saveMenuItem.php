<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
require 'Database.php';

class saveMenuItem {

    public $db;

    public function __construct($post, $get) {		
        $inputData = file_get_contents("php://input");
        $this->inputData = json_decode($inputData);	
        $this->post = $post;
        $this->get = $get;
        $this->menuItemId = ( array_key_exists('id', $this->post) ) ? $this->post['id'] : false;
        $this->db = new Database();
        $this->saveActions();
        $this->db->close();
    }

    public function saveActions() {
        if (count($this->inputData->idsToDelete) > 0 ){
            $this->deleteRows();
        }else if ($this->menuItemId === false  && count($this->post)>0 ) {
            $this->addItem();
        } else {
            $this->updatItem();
        }
    }

    public function updatItem() {
        
        $partialQuery = sprintf("update menu set "
                         . "title='%s',position='%s',visible='%s'"
                         . " where id='%s'", 				
                         $this->escape_string($this->post['mTitle']),                       
                         $this->post['mPosition'],
                         $this->post['mVisible']==="true"?1:0,                        
                         $this->menuItemId
                        );
        
        $fullQuery = sprintf("update menu set "
                         . "description='%s',content='%s',viewContent='%s',viewPages='%s',gallery='%s' "
                         . " where id='%s'",
                         $this->post['mDescription'],
                         $this->post['mContent'],
                         $this->post['mViewContent'],
                         $this->post['mViewPages'],
                         $this->post['mGallery'],
                         $this->menuItemId
                        );
        $query = ($this->post['mDescription'])? $fullQuery : $partialQuery;
        $result =  $this->db->query($query);
        $message = $result ? "updated".$query:$this->db->getError();
        $this->returnResult($result,$message);
    }

    public function addItem() {
        /*id,title,position,visible,description,content,viewContent,viewPages,gallery*/
        $query = sprintf("insert into menu "
                         . "(title,position,visible,description,content,viewContent,viewPages,gallery) "
                         . "values ('%s','%s','%s','%s','%s','%s','%s','%s') ",                          
                         $this->escape_string($this->post['mTitle']),
                         $this->post['mPosition'],
                         $this->post['mVisible']==="true"?1:0,
                         NULL, 
                         NULL,
                         0,
                         0,
                         NULL
                        );
        $result =  $this->db->query($query);
        $message = $result ? "added":$this->db->getError();
        $this->returnResult($result,$message);
    }

    public function deleteRows(){
        $ids = implode(",", $this->inputData->idsToDelete);
        $query = "delete from menu where id in ($ids) ";
        $result =  $this->db->query($query);
        $message = $result ? "deleted":$this->db->getError();
        $this->returnResult($result,$message);
    }

    public function returnResult($result, $message) {
        $data = array('success' => $result? true:false, 'errorMessage' => $message);
        echo json_encode($data);
    }

    public function escape_string($string) {
        return mysqli_real_escape_string($this->db->connection, $string);
    }

}

return new saveMenuItem($_POST, $_GET);