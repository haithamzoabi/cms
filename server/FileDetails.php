<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
session_start();
require 'Database.php';

class FileDetails {
    
    public $filePath;
    public $post;
    public $db;
    public $searchTerm;
  
    public function __construct($filePath,$POST,$searchTerm){        
        require_once('Database.php');
        $this->db = new Database();
        $this->filePath = $filePath;
        $this->post = $POST;
        $this->searchTerm = $searchTerm;
    }
    
    private function checkIfExist(){
        $path =  urlencode($this->filePath );
        $query = "select * from files where location = '$path' limit 1 ";
        $result = $this->db->query($query);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)){
            return $row;
        }
        return false;
        
    }
    
    public function fetchFileData(){
       $rowExist = $this->checkIfExist();
        if ( $rowExist ){
            $locationURL = str_replace("\\","/",urldecode($rowExist["location"]));
            $rowExist["fileLink"] = str_replace($_SERVER["DOCUMENT_ROOT"] , $_SERVER["HTTP_HOST"] ,  $locationURL);           
            return $this->returnResult($rowExist, "", null);
        }
        return $this->returnResult("", "textKeys.FILE_DATA_IS_NOT_AVAILABLE", null);
    }
    
    public function routeQuery(){
        $func = $this->checkIfExist() ? 'saveFileData' : 'insertFileData';
        return $this->$func();
    }
    
    private function insertFileData(){
        $path = urlencode($this->filePath );
        $queryString = "insert into files (name,location,desc1,desc2,details,tagIds) values ('%s','%s','%s','%s','%s','%s')";
        $query = sprintf($queryString,
            $this->escape_string($this->post['name']),
            strval($path),
            $this->escape_string($this->post['desc1']),
            $this->escape_string($this->post['desc2']),
            $this->escape_string($this->post['details']),
            'NULL'
        );
        $result = $this->db->query($query);
        $message = $result ? "": $this->db->getError();
        return $this->returnResult($result,$message,$this->post);
    }
    
    private function saveFileData(){
        $path = urlencode($this->filePath );
        $queryString = "update files set name='%s', desc1='%s', desc2='%s', details='%s', tagIds='%s' where location='%s' ";
        $query = sprintf($queryString ,
                         $this->escape_string($this->post['name']),  
                         $this->escape_string($this->post['desc1']), 
                         $this->escape_string($this->post['desc2']), 
                         $this->escape_string($this->post['details'])
                         ,'' , $path);
        $result = $this->db->query($query);
        $message = $result ? "" : $this->db->getError();
        return $this->returnResult($result,$message,$this->post);
    }
    
    public function DeleteFile(){
        $path = urlencode($this->filePath );
        $query = "delete from files where location = '$path' ";
        $result =  $this->db->query($query);
        $message = $result ?"":$this->db->getError();
        return $this->returnResult($result,$message,array());
    }
    
    public function returnResult($result, $message, $resultData) {
        $data = array('success' => empty($message), 'errorMessage' => $message);
        $resultData = empty($resultData) ? $result : $resultData;
        if (!empty($result)){
            $data = array_merge($data, array("data"=>$resultData));
        }
        return json_encode($data);
    }
    
    public function escape_string($string) {
        return mysqli_real_escape_string($this->db->connection, $string);
    }
    
    public function getDataByTerm($fieldToSearch){
        $dt = array();
        $term = $this->searchTerm;
        $query = "select `$fieldToSearch` from files where `$fieldToSearch` like '%$term%' group by `$fieldToSearch` order by `$fieldToSearch` ";
        $result = $this->db->query($query);
        while ($row = $result->fetch_array(MYSQLI_NUM)){
            array_push($dt,$row[0]);
        }
        return $dt;
    }
    
};

if ($_GET['filePath'] != '' ){
    
    $path = $_GET['filePath'];
    parse_str( file_get_contents( "php://input" ) , $post_vars );
    $details = new FileDetails($path, $post_vars,'');
    if ($_SERVER['REQUEST_METHOD'] == 'GET' ){
        header('Content-Type: application/json');
        echo $details->fetchFileData();
    } 
    
    if ( $_SERVER['REQUEST_METHOD']=='PUT' ){
        header('Content-Type: application/json');
        echo $details->routeQuery();
    }

    if ( $_SERVER['REQUEST_METHOD']=='DELETE' ){
        echo $details->deleteFile();
    }


} else if ( $_GET['term'] != '' ) {
    $details = new FileDetails('','',$_GET['term']);
    header('Content-Type: application/json');
    echo json_encode( $details->getDataByTerm($_GET['field']) );
} else {
    
    $data = array('success' =>'false', 'errorMessage' => 'textKeys.INCORRECT_FILE_PATH');
    echo json_encode($data);
    
}




?>