<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0" ng-if="authenticated">

    <div class="topbar">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#/">{{'textKeys.appName'|xlat}}</a>
        </div>


        <ul class="nav navbar-top-links navbar-left">

           <li>
               <a href="#/filemanager">
                   <i class="fa fa-suitcase fa-fw"></i> {{"textKeys.fileManager"|xlat}}
                </a>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="">
                    <i class="fa fa-user fa-fw"></i> {{username}} <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="#/settings/profile"><i class="fa fa-user fa-fw"></i> {{'textKeys.userProfile'|xlat}}</a>
                    </li>
                    <li>
                        <a href="#/settings/website-settings"><i class="fa fa-gear fa-fw"></i> {{'textKeys.webSettings'|xlat}}</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="" ng-click="logout()"><i class="fa fa-sign-out fa-fw"></i> {{'textKeys.logout'|xlat}}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>


    <div id="navbar" class="sidebar-nav navbar-collapse collapse navbar-default sidebar" role="navigation">
        <ul class="nav" id="side-menu" id="accordion" >
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="{{'textKeys.search'|xlat}}">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </li>
            <li>
                <a ng-repeat="item in menuItems" href="#{{item.link}}">
                    <i class="fa {{item.icon}}"></i>
                    <span ng-bind="item.name | xlat"></span>
                    <span ng-bind="item.displayName"></span>
                </a>
            </li>
        </ul>
    </div>

</nav>
