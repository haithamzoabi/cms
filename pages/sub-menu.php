<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{'textKeys.settings.menu.panel.title'|xlat}}</h3>
    </div>
    <div class="panel-body padding-0">
        <ul class="nav sub-menu"  >
            <li ng-repeat="item in subMenuItems">
                <a href="#{{item.link}}" ng-class ="{selected: item.name == param}">
                    {{item.text}}
                </a>
            </li>
        </ul>
    </div>
</div>
