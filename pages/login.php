<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{'textKeys.systemLogin'|xlat}}</h3>
                </div>
                <div class="panel-body">
                    <form role="form" name="loginForm" rc-submit="login()" method="POST" novalidate>
                        <fieldset>
							<div class="alert alert-danger" role="alert" ng-show="errorMessage">{{errorMessage}}</div>
                            <div class="form-group" ng-class="{'has-error': rc.loginForm.needsAttention(loginForm.email)}" >
                                <input class="form-control" placeholder="{{'textKeys.email'|xlat}}" name="email" type="email" ng-model="credentials.email" autofocus required>
                                <span class="help-block login-help" ng-show="rc.loginForm.needsAttention(loginForm.email)"  >{{'textKeys.fieldRequired'| xlat }}</span>
                            </div>
                            <div class="form-group" ng-class="{'has-error': rc.loginForm.needsAttention(loginForm.password)}">
                                <input class="form-control" placeholder="{{'textKeys.password'|xlat}}" name="password" type="password" value="" ng-model="credentials.password" required>
                                <span class="help-block login-help" ng-show="rc.loginForm.needsAttention(loginForm.password)"  >{{'textKeys.fieldRequired'| xlat }}</span>
                            </div>
                            <div class="checkbox">
                                <label>                                    
                                    <input name="remember" type="checkbox" ng-model="credentials.remeberme" class="checkbox-fix-margin">
                                    <span class="pull-right">{{'textKeys.rememberMe'|xlat}}</span>
                                </label>
                            </div>
                            
                             <button type="submit" class="btn btn-lg btn-success btn-block" value="Login" title="Login"  >
                                <span class="glyphicon glyphicon-log-in" style="top:2px" ></span>&nbsp;{{'textKeys.login'| xlat}}
                            </button>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>