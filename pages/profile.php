<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{'textKeys.profile.panel.title'|xlat}}</h3>
    </div>
    <div class="panel-body">
        <form name="profileForm" class="col-md-5" ng-submit="update()">
            <div ng-show="messageSettings.show &&messageSettings.form == 'profileForm'"ng-bind="messageSettings.text"  ng-class="messageSettings.type" class="alert "></div>
            <div class="form-group ">
                <label for="name">{{'textKeys.name'|xlat}}</label>
                <input type="text" name="name" class="form-control" ng-model="userData.name" id="name" required />
            </div>
            <div class="form-group ">
                <label for="userName">{{'textKeys.username'|xlat}}</label>
                <input type="text" ng-model="userData.userName" class="form-control" name="userName" id="userName" required />
            </div>
            <div class="form-group">
                <label for="userEmail">{{'textKeys.email'|xlat}}</label>
                <input type="email" ng-model="userData.email" name="email" class="form-control" id="userEmail" required />
            </div>
            <input type="submit" value ="{{'textKeys.save'|xlat}}"    class="btn btn-default btn-md btn-primary "/>
        </form>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{'textKeys.settings.panel.title'|xlat}}</h3>
    </div>
    <div class="panel-body">
        <form name="passwordForm" ng-submit="changePassword()"  class="col-md-5">
            <div ng-show="messageSettings.show &&messageSettings.form == 'passwordForm'"ng-bind="messageSettings.text"  ng-class="messageSettings.type" class="alert "></div>
            <div class="form-group">
                <label for="oldPassword">{{'textKeys.old.password'|xlat}}</label>
                <input  required name="oldPassword" type="password"  ng-model="userPssword.oldPassword" class="form-control" id="oldPassword"/>
            </div>
            <div class="form-group">
                <label for="newPassword">{{'textKeys.new.password'|xlat}}</label>
                <input required type="password" ng-model="userPssword.newPassword" name="newPassword"  class="form-control" id="newPassword"/>
            </div>
            <div class="form-group">
                <label for="confirmedPassword">{{'textKeys.confirm.password'|xlat}}</label>
                <input required type="password" ng-model="userPssword.confirmedPassword" name="confirmedPassword"  class="form-control" id="confirmedPassword" />
            </div>
            <input type="submit"  value ="{{'textKeys.save'|xlat}}"   class="btn btn-default btn-md btn-primary " />
        </form>
    </div>
</div>
