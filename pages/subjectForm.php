<form name="subjectForm" class="css-form form-inline" ng-controller="SubjectFormController" rc-submit="saveSubject()" novalidate>
    <div class="alert alert-success fade in" role="alert" ng-show="alertMessage">				
        <span class="glyphicon glyphicon-ok-sign"></span>				
        {{alertMessage}}
        <button type="button" ng-click="resetAlerMessage()" class="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="alert alert-danger" role="alert" ng-show="rc.subjectForm.attempted && rc.subjectForm.needsAttention() || errorMessage">
        <span class="glyphicon glyphicon-remove-sign"></span>
        {{errorMessage}}
    </div>

    <div class="form-group" >
        <label class="col-sm-1 control-label" >{{'textKeys.contentType'|xlat}}:</label>
        <div class="col-sm-11 col-md-8">
            <select class="form-control" name="type" ng-model="subjectData.type" ng-options="option.text for option in typeOptions" ng-change="onTypeChnage()"></select>
        </div>
    </div>


    <div class="form-group" ng-class="{'has-error':rc.subjectForm.needsAttention(subjectData.title)}">
        <label class="col-sm-1 control-label">{{'textKeys.title'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >            
            <div class="input-group"  style="width:100%;">
                <input type="text" class="form-control"  name="title" ng-model="subjectData.title" maxlength="90" required/>
                <span class="input-group-addon" ng-bind="90-subjectData.title.length" style="width:50px;"></span>
            </div>
        </div>
    </div>

    <div class="form-group" ng-class="{'has-error':rc.subjectForm.needsAttention(subjectData.description)}">
        <label class="col-sm-1 control-label">{{'textKeys.description'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="input-group"  style="width:100%;">
                <textarea class="form-control" style="min-height:95px" name="description" ng-model="subjectData.description" maxlength="400" resizable="false" required resizable="false"></textarea>
                <span class="input-group-addon" ng-bind="400-subjectData.description.length" style="width:50px;"></span>
            </div>
        </div>
    </div>


    <div class="form-group" ng-show="contentVisible">
        <label class="col-sm-1 control-label">{{'textKeys.content'| xlat}}:</label>
        <div class="col-sm-11 col-md-8">
            <textarea summernote="true" config="summernoteOptions" ng-model="subjectData.content"></textarea>
        </div>
    </div>

    <div class="form-group" ng-show="galleryVisbile">
        <label class="col-sm-1 control-label">{{'textKeys.image1'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="input-group" >                            
                <input type="text" class="form-control" placeholder="{{'textKeys.chooseFile'|xlat}}" ng-model="subjectData.image1" ng-readonly="!image1IsDisabled" id="subjectDataImage1" >
                <span class="input-group-btn">                                
                    <button class="btn btn-default" type="button" ng-click="openFileManager('image1','files',subjectData)">{{'textKeys.browse'|xlat}}</button>
                    <input type="checkbox" ng-model="image1IsDisabled" class="hidden">
                    <button class="btn btn-default" type="button" ng-click="image1IsDisabled=!image1IsDisabled"><i class="fa" ng-class="image1IsDisabled? 'fa-unlock': 'fa-lock'"   ></i></button>
                </span>
            </div>
        </div>
    </div>    

    <div class="form-group" ng-show="galleryVisbile">
        <label class="col-sm-1 control-label">{{'textKeys.image2'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="input-group" >                            
                <input type="text" class="form-control" placeholder="{{'textKeys.chooseFile'|xlat}}" ng-model="subjectData.image2" ng-readonly="!image2IsDisabled" id="subjectDataImage2" >
                <span class="input-group-btn">                                
                    <button class="btn btn-default" type="button" ng-click="openFileManager('image2','files',subjectData)">{{'textKeys.browse'|xlat}}</button>
                    <input type="checkbox" ng-model="image2IsDisabled" class="hidden">
                    <button class="btn btn-default" type="button" ng-click="image2IsDisabled=!image2IsDisabled"><i class="fa" ng-class="image2IsDisabled? 'fa-unlock': 'fa-lock'"   ></i></button>
                </span>
            </div>
        </div>
    </div>


    <div class="form-group" ng-show="galleryVisbile">
        <label class="col-sm-1 control-label">{{'textKeys.folder'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="input-group" >                            
                <input type="text" class="form-control" placeholder="{{'textKeys.chooseFolder'|xlat}}" ng-model="subjectData.gallery" id="subjectDataGallery" readonly>
                <span class="input-group-btn">                                
                    <button class="btn btn-default" type="button" ng-click="openFileManager('gallery',null,subjectData)">{{'textKeys.browse'|xlat}}</button>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">{{'textKeys.visible'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="btn-group">
                <label class="btn btn-default" ng-model="subjectData.visible" btn-radio="true" uncheckable><span class="fa fa-check"></span></label>
                <label class="btn btn-default" ng-model="subjectData.visible" btn-radio="false" uncheckable><span class="fa fa-times"></span></label>
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-1"></div>
        <div class="col-sm-11 col-md-8">
            <button type="submit" class="btn btn-primary">{{'textKeys.save'| xlat}}</button>
        </div>
    </div>

</form>
