<div ng-if="authenticated" class="posts-table" ng-controller="PostsTableController">


    <div id="toolbar" >
        <div class="btn-group" role="group" aria-label="Tools">				
            <button class="btn btn-default btn-sm" ng-click="addPost()" tooltip="{{addBtnTooltip}}">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                <span class="hidden-xs">{{ 'textKeys.add' | xlat }}</span>
            </button>
            <button class="btn btn-default btn-sm" ng-disabled="selectedItems.length !== 1" ng-click="editPost()">
                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> 
                <span class="hidden-xs">{{ 'textKeys.edit' | xlat }}</span>
            </button>
            <button class="btn btn-default btn-sm" ng-disabled="selectedItems.length === 0" ng-click="deletePost()">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> 
                <span class="hidden-xs">{{ 'textKeys.delete' | xlat }}</span>
            </button>				
        </div>
        <button class="btn btn-default btn-sm btn-sm hidden-xs hidden-sm" ng-click="csv.generate()" ng-href="{{ csv.link()}}"  >
            <span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span>
            <span>{{ 'textKeys.importToExcel' | xlat }}</span>
        </button>
    </div>


    <table init-table="postsTableOtions" ng-model="tableParams" id="posts-table"  export-csv="csv" data-toolbar="#toolbar"></table>

</div>