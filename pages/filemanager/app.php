<div class="panel panel-default filemanager-app">
	<div class="panel-heading" ng-include="'pages/filemanager/toolbar.html'" ></div>
	<div class="panel-body">
		<div class="col-sm-3">

			<div class="panel panel-default treeview" ng-if="!showFileInfo" >
				<div class="panel-heading">{{'textKeys.folders'|xlat}}</div>
				<div class="panel-body" style="max-height:{{viewHeight+18}}px;">
					<div
						 data-angular-treeview="true"
						 data-tree-id="managerTreeView"
						 data-tree-model="treedata"
						 data-node-id="id"
						 data-node-label="text"
						 data-node-children="children" >
					</div>
				</div>
			</div>

			<div class="panel panel-default treeview" ng-if="showFileInfo" ng-include="fileDetailsTemplate" ></div>


		</div>
		<div class="col-sm-9" >
			<div class="panel panel-default filesview">
				<div class="panel-heading">
					<span>{{'textKeys.files'|xlat}}</span>

					<ol class="breadcrumb visible-lg-inline" style="margin-bottom:0px" >
						<li ng-repeat="dir in fileNavigator.currentDisplayPathArray" >
							<a>
								<span ng-if="$first" class="glyphicon glyphicon-home"></span>
								<span ng-hide="$first" >{{dir}}</span>
							</a>
						</li>
					</ol>
				</div>
				<div class="panel-body" ng-include="filesview"></div>
			</div>

		</div>
	</div>
	<div class="panel-footer" >
		<button type="button" ng-if="isModal" ng-click="$close(selectionPath)" ng-disabled="selectionPath==''" class="btn btn-sm btn-primary">{{'textKeys.select'|xlat}}</button>
		&nbsp;
	</div>

	<div ng-include="'pages/filemanager/modals.html'"></div>
	<div ng-include="'pages/filemanager/item-context-menu.html'"></div>

</div>