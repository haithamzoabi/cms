<form name="itemForm" rc-submit="save()" class="css-form form-inline" novalidate>
    <div class="alert alert-success fade in" role="alert" ng-show="alertMessage">				
        <span class="glyphicon glyphicon-ok-sign"></span>				
        {{alertMessage}}
        <button type="button" ng-click="resetAlerMessage()" class="close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="alert alert-danger" role="alert" ng-show="rc.itemForm.attempted && rc.itemForm.needsAttention()">
        <span class="glyphicon glyphicon-remove-sign"></span>
        {{errorMessage}}
    </div>
    <div class="form-group" ng-class="{'has-error':rc.itemForm.needsAttention(itemForm.mDescription)}">
        <label class="col-sm-1 control-label">{{'textKeys.description'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <textarea class="form-control" style="width:100%;min-height:95px" name="mDescription" ng-model="menuData.mDescription" resizable="false" required></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">{{'textKeys.content'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >					
            <div class="btn-group">
                <label class="btn btn-default" ng-model="menuData.mViewContent" btn-radio="'1'" uncheckable><span class="fa fa-check"></span></label>
                <label class="btn btn-default" ng-model="menuData.mViewContent" btn-radio="'0'" uncheckable><span class="fa fa-times"></span></label>
            </div>
        </div>
    </div>


    <div class="form-group" ng-show="menuData.mViewContent == '1'">
        <span class="col-sm-1 control-label"></span>
        <div class="col-sm-11 col-md-8">
            <textarea summernote="true" config="summernoteOptions" ng-model="menuData.mContent"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">{{'textKeys.subjects'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="btn-group">
                <label class="btn btn-default" ng-model="menuData.mViewPages" btn-radio="'1'" uncheckable><span class="fa fa-check"></span></label>
                <label class="btn btn-default" ng-model="menuData.mViewPages" btn-radio="'0'" uncheckable><span class="fa fa-times"></span></label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">{{'textKeys.gallery'| xlat}}:</label>
        <div class="col-sm-11 col-md-8" >
            <div class="btn-group">
                <label class="btn btn-default" ng-model="menuData.mGalleryCheck" btn-radio="true" uncheckable><span class="fa fa-check"></span></label>
                <label class="btn btn-default" ng-model="menuData.mGalleryCheck" ng-click="menuData.mGallery=''" btn-radio="false" uncheckable><span class="fa fa-times"></span></label>
            </div>
            <div class="input-group" ng-show="menuData.mGalleryCheck">                            
                <input type="text" class="form-control" placeholder="{{'textKeys.chooseFolder'|xlat}}" ng-model="menuData.mGallery" id="menuDatamGallery" readonly>
                <span class="input-group-btn">                                
                    <button class="btn btn-default" type="button" ng-click="openFileManager('mGallery')">{{'textKeys.browse'|xlat}}</button>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-1"></div>
        <div class="col-sm-11 col-md-8">
            <button type="submit" class="btn btn-primary">{{'textKeys.save'| xlat}}</button>
        </div>
    </div>

</form>