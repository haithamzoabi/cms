<div ng-if="authenticated">

    <div class="row" ng-controller="MenuItemController as mic">
        <h1 class="page-header">{{menuData.mTitle}}</h1>
        <div class="page-content">

            <div class="panel with-nav-tabs panel-default menuItemTabPanel">
                <div class="panel-heading">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" ng-repeat="tab in mic.tabs track by $index" ng-class="{'active':tab.active}" ng-if="tab.isVisible" id="{{tab.id}}">
                            <a ng-href="#{{tab.href}}" role="tab">
                                {{tab.title}}
                                <span ng-if="tab.closable" class="close small" ng-click="onCloseTabClick(tab,$event , $index)" data-dismiss="presentation">&times;</span>
                            </a>
                        </li>                   
                    </ul>
                </div>
                <div class="panel-body">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane"  ng-repeat="tab in mic.tabs track by $index" ng-class="{'active':tab.active}" ng-include="tab.active?tab.content:''"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="js/lib/bootstrap-tabdrop.js" type="text/javascript"></script>
