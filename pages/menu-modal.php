<form name="menuForm" class="css-form form-inline" rc-submit="save()" novalidate>	
	<div class="modal-header">
		<button type="button" class="close" ng-click="cancel()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h3 class="modal-title" >{{'textKeys.menuItemDetails'| xlat}}</h3>
	</div>
	<div class="modal-body">
		<div class="alert alert-danger" role="alert" ng-show="rc.menuForm.attempted && rc.menuForm.needsAttention()">
			{{errorMessage}}
		</div>

		<div class="form-group" ng-class="{'has-error':menuForm.mTitle.$dirty && menuForm.mTitle.$error.required}">
			<label class="col-sm-3 control-label">{{'textKeys.title'| xlat}}:</label>
            <div class="col-sm-9" >
                <input type="text" class="form-control" name="mTitle" ng-model="menuData.mTitle" required>
            </div>
		</div>
		<div class="form-group" ng-class="{'has-error':menuForm.mPosition.$error.number || menuForm.mPosition.$dirty && menuForm.mPosition.$error.required}">
			<label class="col-sm-3 control-label">{{'textKeys.menuPosition'| xlat}}:</label>
            <div class="col-sm-9" >
                <input type="number" min="0" class="form-control" name="mPosition" ng-model="menuData.mPosition" required>
            </div>
		</div>

        <div class="form-group">
			<label class="col-sm-3 control-label">{{'textKeys.visbibleMenu'| xlat}}:</label>
            <div class="col-sm-9" >
                <div class="btn-group">
                    <label class="btn btn-default" ng-model="menuData.mVisible" btn-radio="true" uncheckable><span class="fa fa-check"></span></label>
                    <label class="btn btn-default" ng-model="menuData.mVisible" btn-radio="false" uncheckable><span class="fa fa-times"></span></label>
                </div>
            </div>
		</div>

	</div>


</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" ng-click="cancel()" >{{'textKeys.cancel'| xlat}}</button>
    <button type="submit" class="btn btn-primary">{{'textKeys.save'| xlat}}</button>
</div>
</form>