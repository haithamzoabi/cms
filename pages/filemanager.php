<div ng-if="authenticated">

    <div class="row">
		<h1 class="page-header">{{'textKeys.fileManager'|xlat}}</h1>
		<div class="page-content">

            <filemanager></filemanager>
            
		</div>
    </div>

</div>