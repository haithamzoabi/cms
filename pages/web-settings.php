<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{'textKeys.webSettings.panel.title'|xlat}}</h3>
    </div>
    <div class="panel-body">
        <form name="webSettingsForm" class="col-md-5" ng-submit="updateWebSettings()">
            <div ng-show="messageSettings.show &&messageSettings.form == 'webSettingsForm'"ng-bind="messageSettings.text"  ng-class="messageSettings.type" class="alert "></div>
            <div class="form-group ">
                <label for="webName">{{'textKeys.webName'|xlat}}</label>
                <input type="text" name="webName" class="form-control" ng-model="webData.name" id="webNameInput" required />
            </div>
            <div class="form-group ">
                <label for="webLink">{{'textKeys.webLink'|xlat}}</label>
                <input type="text" ng-model="webData.link" class="form-control" name="webLink" id="webLinkInput" required />
            </div>
            <div class="form-group ">
                <label for="webAnalysisCode">{{'textKeys.webAnalysis'|xlat}}</label>
                <input type="text" ng-model="webData.analysisCode" class="form-control" name="webAnalysisCode" id="webAnalysisCodeInput" required />
            </div>
            <div class="form-group ">
                <label for="webAnalysisCode">{{'textKeys.languages'|xlat}}</label>
                <select  ng-model="webData.language" name="languages"  class="form-control select-input-style languages-select-input">
                    <option value="he">{{'textKeys.languages.hebrew'|xlat}}</option>
                    <option value="ar">{{'textKeys.languages.arabic'|xlat}}</option>
                </select>
            </div>
            <input type="submit" value ="{{'textKeys.save'|xlat}}"    class="btn btn-default btn-md btn-primary "/>
        </form>
    </div>
</div>

