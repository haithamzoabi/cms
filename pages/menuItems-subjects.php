<div ng-if="authenticated" class="subjects-table" ng-controller="SubjectsTableController">


    <div id="toolbar" >
        <div class="btn-group" role="group" aria-label="Tools">				
            <button class="btn btn-default btn-sm" ng-click="addSubject()">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
                <span class="hidden-xs">{{ 'textKeys.add' | xlat }}</span>
            </button>
            <button class="btn btn-default btn-sm" ng-disabled="selectedItems.length !== 1" ng-click="editSubject()">
                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> 
                <span class="hidden-xs">{{ 'textKeys.edit' | xlat }}</span>
            </button>
            <button class="btn btn-default btn-sm" ng-disabled="selectedItems.length === 0" ng-click="deleteSubject()">
                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> 
                <span class="hidden-xs">{{ 'textKeys.delete' | xlat }}</span>
            </button>				
        </div>
        <button class="btn btn-default btn-sm btn-sm hidden-xs hidden-sm" ng-click="csv.generate()" ng-href="{{ csv.link()}}"  >
            <span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span>
            <span>{{ 'textKeys.importToExcel' | xlat }}</span>
        </button>
    </div>


    <table init-table="subjectsTableOtions" ng-model="tableParams" id="subjects-table"  export-csv="csv" data-toolbar="#toolbar"></table>

</div>