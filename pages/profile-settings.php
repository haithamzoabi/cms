<div class="row" ng-if="authenticated" ng-controller="SettingsController">
    <h1 class="page-header">{{'textKeys.profile.settings'|xlat}}</h1>
    <div ng-if="pageToInclude">
        <div ng-include="'pages/sub-menu.php'" class="col-sm-4 col-md-3 col-lg-2"></div>
        <div class="col-sm-8 col-md-9 col-lg-10">
            <div class="" ng-include="pageToInclude"></div>
        </div>
    </div>
    <div ng-if="!pageToInclude" class="col-md-12" ng-include="'pages/blankPage.php'"></div>

</div>
